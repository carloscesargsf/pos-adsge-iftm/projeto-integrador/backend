package com.example.demo.exceptions;

public class NotAuthorizedException extends BaseException {

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(String message, Object... params) {
        super(message, params);
    }

}
