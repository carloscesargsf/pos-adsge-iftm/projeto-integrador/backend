package com.example.demo.exceptions;

import com.example.demo.exceptions.info.ExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

import static com.example.demo.constants.ExceptionMessages.INVALID_METHOD_ARGUMENT;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ExceptionMessage> entityNotFoundException(EntityNotFoundException e,
                                                                    HttpServletRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessage(Instant.now(),
                HttpStatus.NOT_FOUND.value(), e.getMessage(), request.getRequestURI());

        return generateResponseEntity(exceptionMessage);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ExceptionMessage> illegalArgumentException(IllegalArgumentException e,
                                                                     HttpServletRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessage(Instant.now(),
                HttpStatus.BAD_REQUEST.value(), e.getMessage(), request.getRequestURI());

        return generateResponseEntity(exceptionMessage);
    }

    @ExceptionHandler(NotAuthorizedException.class)
    public ResponseEntity<ExceptionMessage> notAuthorizedException(NotAuthorizedException e,
                                                                   HttpServletRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessage(Instant.now(),
                HttpStatus.UNAUTHORIZED.value(), e.getMessage(), request.getRequestURI());

        return generateResponseEntity(exceptionMessage);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionMessage> methodArgumentNotValidException(MethodArgumentNotValidException e,
                                                                            HttpServletRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessage(Instant.now(),
                HttpStatus.UNPROCESSABLE_ENTITY.value(),
                INVALID_METHOD_ARGUMENT, request.getRequestURI());

        for (FieldError fe : e.getBindingResult().getFieldErrors()) {
            exceptionMessage.addError(fe.getField(), fe.getDefaultMessage());
        }

        return generateResponseEntity(exceptionMessage);
    }

    private ResponseEntity<ExceptionMessage> generateResponseEntity(ExceptionMessage exceptionMessage) {
        return ResponseEntity.status(exceptionMessage.getStatus()).body(exceptionMessage);
    }

}
