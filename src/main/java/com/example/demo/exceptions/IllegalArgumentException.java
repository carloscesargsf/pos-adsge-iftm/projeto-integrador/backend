package com.example.demo.exceptions;

public class IllegalArgumentException extends BaseException {

    public IllegalArgumentException(String message) {
        super(message);
    }

    public IllegalArgumentException(String message, Object... params) {
        super(message, params);
    }

}
