package com.example.demo.exceptions;

public class EntityNotFoundException extends BaseException {

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Object... params) {
        super(message, params);
    }

}
