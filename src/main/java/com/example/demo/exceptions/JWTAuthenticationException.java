package com.example.demo.exceptions;

public class JWTAuthenticationException extends BaseException {

    public JWTAuthenticationException(String message) {
        super(message);
    }

    public JWTAuthenticationException(String message, Object... params) {
        super(message, params);
    }

}
