package com.example.demo.exceptions;

public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Object... params) {
        this(params.length > 0 ? String.format(message, params) : message);
    }

}
