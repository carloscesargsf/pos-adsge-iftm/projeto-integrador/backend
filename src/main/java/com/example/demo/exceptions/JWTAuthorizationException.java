package com.example.demo.exceptions;

public class JWTAuthorizationException extends BaseException {

    public JWTAuthorizationException(String message) {
        super(message);
    }

    public JWTAuthorizationException(String message, Object... params) {
        super(message, params);
    }

}
