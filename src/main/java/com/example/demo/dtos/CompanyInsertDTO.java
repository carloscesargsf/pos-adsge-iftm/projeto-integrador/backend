package com.example.demo.dtos;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.validation.constraints.NotBlank;

public class CompanyInsertDTO {

    @NotBlank
    @Length(min = 1, max = 200)
    private String companyName;

    @NotBlank
    @Length(min = 1, max = 200)
    private String tradingName;

    @NotBlank
    @CNPJ
    private String cnpj;

    private PersonInsertDTO admin;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public PersonInsertDTO getAdmin() {
        return admin;
    }

    public void setAdmin(PersonInsertDTO admin) {
        this.admin = admin;
    }

}
