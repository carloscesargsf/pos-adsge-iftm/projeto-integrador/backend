package com.example.demo.dtos;

import com.example.demo.filters.PageInfo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PaginatedItemsDTO<E, D> {

    private List<D> content;

    private Integer pageNumber;

    private Integer pageSize;

    private Long totalElements;

    private Integer totalPages;

    private String[] sortBy;

    private Boolean ascending;

    public PaginatedItemsDTO(List<D> dto) {
        content = dto;
    }

    public PaginatedItemsDTO(Page<E> page, PageInfo pageInfo, Function<E, D> mapper) {
        content = page.stream().map(e -> mapper.apply(e)).collect(Collectors.toList());
        pageNumber = page.getNumber();
        pageSize = page.getSize();
        totalElements = page.getTotalElements();
        totalPages = page.getTotalPages();
        sortBy = pageInfo.getSortBy().orElse(new String[0]);
        ascending = pageInfo.getAscending();
    }

    public List<D> getContent() {
        return content;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Long getTotalElements() {
        return totalElements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public String[] getSortBy() {
        return sortBy;
    }

    public Boolean getAscending() {
        return ascending;
    }

}
