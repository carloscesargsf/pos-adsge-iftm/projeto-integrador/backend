package com.example.demo.dtos;

public class ExamQuestionViewItemDTO {

    private Long id;

    private Long examId;

    private QuestionViewItemDTO question;

    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public QuestionViewItemDTO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionViewItemDTO question) {
        this.question = question;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
