package com.example.demo.dtos;

import java.util.LinkedList;
import java.util.List;

public class CategoryViewItemDTO {

    private Long id;

    private String name;

    private String description;

    private Long parentCategoryId;

    private Boolean active;

    private List<CategoryViewItemDTO> childCategories = new LinkedList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<CategoryViewItemDTO> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<CategoryViewItemDTO> childCategories) {
        this.childCategories = childCategories;
    }

}
