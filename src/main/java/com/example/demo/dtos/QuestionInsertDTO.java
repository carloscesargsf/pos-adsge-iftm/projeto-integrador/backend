package com.example.demo.dtos;

import com.example.demo.validators.CategoryForRelationValidations;
import com.example.demo.validators.QuestionTypeForRelationValidations;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

public class QuestionInsertDTO {

    @NotNull
    @QuestionTypeForRelationValidations
    private Long questionTypeId;

    @NotNull
    @CategoryForRelationValidations
    private Long categoryId;

    @NotBlank
    private String title;

    private List<QuestionPossibleAnswerDTO> possibleAnswers = new LinkedList<>();

    public Long getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(Long questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<QuestionPossibleAnswerDTO> getPossibleAnswers() {
        return possibleAnswers;
    }

    public void setPossibleAnswers(List<QuestionPossibleAnswerDTO> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }

}
