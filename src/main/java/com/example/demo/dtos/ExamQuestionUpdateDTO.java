package com.example.demo.dtos;

import com.example.demo.validators.ExamForRelationValidations;
import com.example.demo.validators.QuestionForRelationValidations;

import javax.validation.constraints.NotNull;

public class ExamQuestionUpdateDTO {

    @NotNull
    private Long id;

    @NotNull
    @ExamForRelationValidations
    private Long examId;

    @NotNull
    @QuestionForRelationValidations
    private Long questionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

}
