package com.example.demo.dtos;

import com.example.demo.validators.ExamForRelationValidations;
import com.example.demo.validators.RatedForRelationValidations;

import javax.validation.constraints.NotNull;

public class ExamRatedUpdateDTO {

    @NotNull
    private Long id;

    @NotNull
    @ExamForRelationValidations
    private Long examId;

    @NotNull
    @RatedForRelationValidations
    private Long ratedId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getRatedId() {
        return ratedId;
    }

    public void setRatedId(Long ratedId) {
        this.ratedId = ratedId;
    }

}
