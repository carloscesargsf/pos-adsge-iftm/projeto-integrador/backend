package com.example.demo.dtos;

import java.util.LinkedList;
import java.util.List;

public class PersonViewItemDTO {

    private Long id;

    private String name;

    private String cpf;

    private String gender;

    private String email;

    private Boolean active;

    private List<PersonRoleViewItemDTO> personRoles = new LinkedList<>();

    public PersonViewItemDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<PersonRoleViewItemDTO> getPersonRoles() {
        return personRoles;
    }

    public void setPersonRoles(List<PersonRoleViewItemDTO> personRoles) {
        this.personRoles = personRoles;
    }

}
