package com.example.demo.dtos;

import com.example.demo.validators.ExamForRelationValidations;
import com.example.demo.validators.RatedForRelationValidations;

import javax.validation.constraints.NotNull;

public class ExamRatedInsertDTO {

    @NotNull
    @ExamForRelationValidations
    private Long examId;

    @NotNull
    @RatedForRelationValidations
    private Long ratedId;

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getRatedId() {
        return ratedId;
    }

    public void setRatedId(Long ratedId) {
        this.ratedId = ratedId;
    }

}
