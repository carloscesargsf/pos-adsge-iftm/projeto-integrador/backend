package com.example.demo.dtos;

import com.example.demo.validators.ExamQuestionForRelationValidations;
import com.example.demo.validators.ExamRatedForRelationValidations;
import com.example.demo.validators.PossibleAnswerForRelationValidations;

import javax.validation.constraints.NotNull;

public class AnswerUpdateDTO {

    @NotNull
    private Long id;

    @NotNull
    @ExamRatedForRelationValidations
    private Long examRatedId;

    @NotNull
    @ExamQuestionForRelationValidations
    private Long examQuestionId;

    @NotNull
    @PossibleAnswerForRelationValidations
    private Long possibleAnswerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamRatedId() {
        return examRatedId;
    }

    public void setExamRatedId(Long examRatedId) {
        this.examRatedId = examRatedId;
    }

    public Long getExamQuestionId() {
        return examQuestionId;
    }

    public void setExamQuestionId(Long examQuestionId) {
        this.examQuestionId = examQuestionId;
    }

    public Long getPossibleAnswerId() {
        return possibleAnswerId;
    }

    public void setPossibleAnswerId(Long possibleAnswerId) {
        this.possibleAnswerId = possibleAnswerId;
    }

}
