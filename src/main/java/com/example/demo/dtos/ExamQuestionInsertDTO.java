package com.example.demo.dtos;

import com.example.demo.validators.ExamForRelationValidations;
import com.example.demo.validators.QuestionForRelationValidations;

import javax.validation.constraints.NotNull;

public class ExamQuestionInsertDTO {

    @NotNull
    @ExamForRelationValidations
    private Long examId;

    @NotNull
    @QuestionForRelationValidations
    private Long questionId;

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

}
