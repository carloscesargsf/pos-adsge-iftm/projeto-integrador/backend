package com.example.demo.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class ExamViewItemDTO {

    private Long id;

    private Long evaluatorId;

    private String evaluatorName;

    private String evaluatorGender;

    private String name;

    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime beginDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;

    private Double grade;

    private Boolean active;

    private Set<ExamQuestionViewItemDTO> questions = new HashSet<>();

    private Set<ExamRatedViewItemDTO> rated = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEvaluatorId() {
        return evaluatorId;
    }

    public void setEvaluatorId(Long evaluatorId) {
        this.evaluatorId = evaluatorId;
    }

    public String getEvaluatorName() {
        return evaluatorName;
    }

    public void setEvaluatorName(String evaluatorName) {
        this.evaluatorName = evaluatorName;
    }

    public String getEvaluatorGender() {
        return evaluatorGender;
    }

    public void setEvaluatorGender(String evaluatorGender) {
        this.evaluatorGender = evaluatorGender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDateTime beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<ExamQuestionViewItemDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<ExamQuestionViewItemDTO> questions) {
        this.questions = questions;
    }

    public Set<ExamRatedViewItemDTO> getRated() {
        return rated;
    }

    public void setRated(Set<ExamRatedViewItemDTO> rated) {
        this.rated = rated;
    }

}
