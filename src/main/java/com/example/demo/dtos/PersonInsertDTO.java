package com.example.demo.dtos;

import com.example.demo.validators.GenderValidations;
import com.example.demo.validators.PersonInsertDTOValidations;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.LinkedList;
import java.util.List;

@PersonInsertDTOValidations
public class PersonInsertDTO {

    @NotBlank
    @Length(min = 1, max = 200)
    private String name;

    @NotBlank
    @CPF
    private String cpf;

    @NotBlank
    @GenderValidations
    private String gender;

    @NotBlank
    @Email
    @Length(min = 1, max = 200)
    private String email;

    @NotBlank
    @Length(min = 8, max = 200)
    private String password;

    private List<String> roles = new LinkedList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

}
