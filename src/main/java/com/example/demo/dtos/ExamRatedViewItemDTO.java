package com.example.demo.dtos;

public class ExamRatedViewItemDTO {

    private Long id;

    private Long examId;

    private Long ratedId;

    private String ratedName;

    private String ratedGender;

    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getRatedId() {
        return ratedId;
    }

    public void setRatedId(Long ratedId) {
        this.ratedId = ratedId;
    }

    public String getRatedName() {
        return ratedName;
    }

    public void setRatedName(String ratedName) {
        this.ratedName = ratedName;
    }

    public String getRatedGender() {
        return ratedGender;
    }

    public void setRatedGender(String ratedGender) {
        this.ratedGender = ratedGender;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
