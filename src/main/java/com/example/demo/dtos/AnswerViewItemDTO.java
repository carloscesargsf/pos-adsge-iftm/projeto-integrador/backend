package com.example.demo.dtos;

import java.time.LocalDateTime;

public class AnswerViewItemDTO {

    private Long id;

    private Long examId;

    private String examName;

    private LocalDateTime examBeginDate;

    private LocalDateTime examEndDate;

    private Double examGrade;

    private Long ratedId;

    private String ratedName;

    private String ratedGender;

    private Long questionId;

    private String questionTitle;

    private Long possibleAnswerId;

    private String possibleAnswerDescription;

    private Boolean possibleAnswerCorrect;

    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public LocalDateTime getExamBeginDate() {
        return examBeginDate;
    }

    public void setExamBeginDate(LocalDateTime examBeginDate) {
        this.examBeginDate = examBeginDate;
    }

    public LocalDateTime getExamEndDate() {
        return examEndDate;
    }

    public void setExamEndDate(LocalDateTime examEndDate) {
        this.examEndDate = examEndDate;
    }

    public Double getExamGrade() {
        return examGrade;
    }

    public void setExamGrade(Double examGrade) {
        this.examGrade = examGrade;
    }

    public Long getRatedId() {
        return ratedId;
    }

    public void setRatedId(Long ratedId) {
        this.ratedId = ratedId;
    }

    public String getRatedName() {
        return ratedName;
    }

    public void setRatedName(String ratedName) {
        this.ratedName = ratedName;
    }

    public String getRatedGender() {
        return ratedGender;
    }

    public void setRatedGender(String ratedGender) {
        this.ratedGender = ratedGender;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Long getPossibleAnswerId() {
        return possibleAnswerId;
    }

    public void setPossibleAnswerId(Long possibleAnswerId) {
        this.possibleAnswerId = possibleAnswerId;
    }

    public String getPossibleAnswerDescription() {
        return possibleAnswerDescription;
    }

    public void setPossibleAnswerDescription(String possibleAnswerDescription) {
        this.possibleAnswerDescription = possibleAnswerDescription;
    }

    public Boolean getPossibleAnswerCorrect() {
        return possibleAnswerCorrect;
    }

    public void setPossibleAnswerCorrect(Boolean possibleAnswerCorrect) {
        this.possibleAnswerCorrect = possibleAnswerCorrect;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
