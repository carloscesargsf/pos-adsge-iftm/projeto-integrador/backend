package com.example.demo.dtos;

import com.example.demo.validators.CompanyForRelationValidations;
import com.example.demo.validators.PersonForRelationValidations;
import com.example.demo.validators.PersonRoleInsertDTOValidations;
import com.example.demo.validators.RoleValidations;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@PersonRoleInsertDTOValidations
public class PersonRoleInsertDTO {

    @NotNull
    @PersonForRelationValidations
    private Long personId;

    @NotNull
    @CompanyForRelationValidations
    private Long companyId;

    @NotBlank
    @RoleValidations
    private String role;

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
