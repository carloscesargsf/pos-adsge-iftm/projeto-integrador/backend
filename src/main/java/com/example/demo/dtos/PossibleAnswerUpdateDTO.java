package com.example.demo.dtos;

import com.example.demo.validators.QuestionForRelationValidations;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PossibleAnswerUpdateDTO {

    @NotNull
    private Long id;

    @NotNull
    @QuestionForRelationValidations
    private Long questionId;

    @NotBlank
    private String description;

    @NotNull
    private Boolean correct;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

}
