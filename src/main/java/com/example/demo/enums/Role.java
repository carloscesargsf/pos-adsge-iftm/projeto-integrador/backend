package com.example.demo.enums;

import java.util.HashMap;
import java.util.Map;

public enum Role {

    ADMIN("ADMIN", "Admin"),
    EVALUATOR("EVALUATOR", "Evaluator"),
    RATED("RATED", "Rated");

    private String code;

    private String label;

    public static final Map<String, Role> OPTIONS = new HashMap<>();

    static {
        for (Role role : Role.values()) {
            OPTIONS.put(role.getCode(), role);
        }
    }

    Role(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

}
