package com.example.demo.enums;

import java.util.HashMap;
import java.util.Map;

public enum Gender {

    M("M", "Male"),
    F("F", "Female");

    private String code;

    private String label;

    public static final Map<String, Gender> OPTIONS = new HashMap<>();

    static {
        for (Gender gender : Gender.values()) {
            OPTIONS.put(gender.getCode(), gender);
        }
    }

    Gender(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

}
