package com.example.demo.validators;

import com.example.demo.enums.Role;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RoleValidator implements ConstraintValidator<RoleValidations, String> {

    @Override
    public void initialize(RoleValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(String role, ConstraintValidatorContext constraintValidatorContext) {
        return Role.OPTIONS.containsKey(role);
    }

}
