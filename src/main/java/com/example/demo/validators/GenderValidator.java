package com.example.demo.validators;

import com.example.demo.enums.Gender;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GenderValidator implements ConstraintValidator<GenderValidations, String> {

    @Override
    public void initialize(GenderValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(String gender, ConstraintValidatorContext constraintValidatorContext) {
        return Gender.OPTIONS.containsKey(gender);
    }

}
