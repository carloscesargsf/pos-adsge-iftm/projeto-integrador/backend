package com.example.demo.validators;

import com.example.demo.entities.Category;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.CategoryRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryForRelationValidator implements ConstraintValidator<CategoryForRelationValidations, Long> {

    private final CategoryRepository categoryRepository;

    public CategoryForRelationValidator(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void initialize(CategoryForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long categoryId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<Category> category = categoryRepository.findById(categoryId);

        if (!category.isPresent()) {
            fieldMessagesList.add(new FieldMessage("categoryId", "Category informed doesn't exists."));
        } else {
            if (!category.get().getActive()) {
                fieldMessagesList.add(new FieldMessage("categoryId", "Category informed is inactive."));
            }
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
