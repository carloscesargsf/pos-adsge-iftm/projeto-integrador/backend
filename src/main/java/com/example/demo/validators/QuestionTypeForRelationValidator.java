package com.example.demo.validators;

import com.example.demo.entities.QuestionType;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.QuestionTypeRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QuestionTypeForRelationValidator implements ConstraintValidator<QuestionTypeForRelationValidations, Long> {

    private final QuestionTypeRepository questionTypeRepository;

    public QuestionTypeForRelationValidator(QuestionTypeRepository questionTypeRepository) {
        this.questionTypeRepository = questionTypeRepository;
    }

    @Override
    public void initialize(QuestionTypeForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long questionTypeId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<QuestionType> questionType = questionTypeRepository.findById(questionTypeId);

        if (!questionType.isPresent()) {
            fieldMessagesList.add(new FieldMessage("questionTypeId", "Question Type informed doesn't exists."));
        } else {
            if (!questionType.get().getActive()) {
                fieldMessagesList.add(new FieldMessage("questionTypeId", "Question Type informed is inactive."));
            }
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
