package com.example.demo.validators;

import com.example.demo.entities.ExamQuestion;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.ExamQuestionRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExamQuestionForRelationValidator implements ConstraintValidator<ExamQuestionForRelationValidations, Long> {

    private final ExamQuestionRepository examQuestionRepository;

    public ExamQuestionForRelationValidator(ExamQuestionRepository examQuestionRepository) {
        this.examQuestionRepository = examQuestionRepository;
    }

    @Override
    public void initialize(ExamQuestionForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long examQuestionId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<ExamQuestion> examQuestion = examQuestionRepository.findById(examQuestionId);

        if (!examQuestion.isPresent()) {
            fieldMessagesList.add(new FieldMessage("examQuestionId", "ExamQuestion informed doesn't exists."));
        } else if (!examQuestion.get().getActive()) {
            fieldMessagesList.add(new FieldMessage("examQuestionId", "ExamQuestion informed is inactive."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
