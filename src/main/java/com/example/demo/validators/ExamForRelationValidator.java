package com.example.demo.validators;

import com.example.demo.entities.Exam;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.ExamRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExamForRelationValidator implements ConstraintValidator<ExamForRelationValidations, Long> {

    private final ExamRepository examRepository;

    public ExamForRelationValidator(ExamRepository examRepository) {
        this.examRepository = examRepository;
    }

    @Override
    public void initialize(ExamForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long examId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<Exam> exam = examRepository.findById(examId);

        if (!exam.isPresent()) {
            fieldMessagesList.add(new FieldMessage("examId", "Exam informed doesn't exists."));
        } else if (!exam.get().getActive()) {
            fieldMessagesList.add(new FieldMessage("examId", "Exam informed is inactive."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
