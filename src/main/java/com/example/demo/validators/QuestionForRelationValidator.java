package com.example.demo.validators;

import com.example.demo.entities.Question;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.QuestionRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QuestionForRelationValidator implements ConstraintValidator<QuestionForRelationValidations, Long> {

    private final QuestionRepository questionRepository;

    public QuestionForRelationValidator(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public void initialize(QuestionForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long questionTypeId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<Question> question = questionRepository.findById(questionTypeId);

        if (!question.isPresent()) {
            fieldMessagesList.add(new FieldMessage("questionId", "Question informed doesn't exists."));
        } else {
            if (!question.get().getActive()) {
                fieldMessagesList.add(new FieldMessage("questionId", "Question informed is inactive."));
            }
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
