package com.example.demo.validators;

import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.PersonRoleRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EvaluatorForRelationValidator implements ConstraintValidator<EvaluatorForRelationValidations, Long> {

    private final PersonRoleRepository personRoleRepository;

    public EvaluatorForRelationValidator(PersonRoleRepository personRoleRepository) {
        this.personRoleRepository = personRoleRepository;
    }

    @Override
    public void initialize(EvaluatorForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long categoryId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<PersonRole> personRole = personRoleRepository.findById(categoryId);

        if (!personRole.isPresent()) {
            fieldMessagesList.add(new FieldMessage("evaluatorId", "Evaluator informed doesn't exists."));
        } else if (!personRole.get().getRole().equals(Role.EVALUATOR)) {
            fieldMessagesList.add(new FieldMessage("evaluatorId", "Evaluator informed is invalid."));
        } else if (!personRole.get().getActive()) {
            fieldMessagesList.add(new FieldMessage("evaluatorId", "Evaluator informed is inactive."));
        }
        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
