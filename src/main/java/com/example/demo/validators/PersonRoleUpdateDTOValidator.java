package com.example.demo.validators;

import com.example.demo.dtos.PersonRoleUpdateDTO;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.PersonRoleRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonRoleUpdateDTOValidator implements ConstraintValidator<PersonRoleUpdateDTOValidations, PersonRoleUpdateDTO> {

    private final PersonRoleRepository personRoleRepository;

    public PersonRoleUpdateDTOValidator(PersonRoleRepository personRoleRepository) {
        this.personRoleRepository = personRoleRepository;
    }

    @Override
    public void initialize(PersonRoleUpdateDTOValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(PersonRoleUpdateDTO personRoleUpdateDTO, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<PersonRole> personRole = personRoleRepository.findByPersonIdAndCompanyIdAndRole(
                personRoleUpdateDTO.getPersonId(), personRoleUpdateDTO.getCompanyId(), Role.OPTIONS.get(personRoleUpdateDTO.getRole()));

        if (personRole.isPresent() && !personRole.get().getId().equals(personRoleUpdateDTO.getId())) {
            fieldMessagesList.add(new FieldMessage("personId", "Role already defined for this company."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
