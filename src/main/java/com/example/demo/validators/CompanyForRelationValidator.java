package com.example.demo.validators;

import com.example.demo.entities.Company;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.CompanyRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CompanyForRelationValidator implements ConstraintValidator<CompanyForRelationValidations, Long> {

    private final CompanyRepository companyRepository;

    public CompanyForRelationValidator(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public void initialize(CompanyForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long companyId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<Company> company = companyRepository.findById(companyId);

        if (!company.isPresent()) {
            fieldMessagesList.add(new FieldMessage("companyId", "Company informed doesn't exists."));
        } else {
            if (!company.get().getActive()) {
                fieldMessagesList.add(new FieldMessage("companyId", "Company informed is inactive."));
            }
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
