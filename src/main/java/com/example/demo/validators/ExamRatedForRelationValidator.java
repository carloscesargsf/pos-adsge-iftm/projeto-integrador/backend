package com.example.demo.validators;

import com.example.demo.entities.ExamRated;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.ExamRatedRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExamRatedForRelationValidator implements ConstraintValidator<ExamRatedForRelationValidations, Long> {

    private final ExamRatedRepository examRatedRepository;

    public ExamRatedForRelationValidator(ExamRatedRepository examRatedRepository) {
        this.examRatedRepository = examRatedRepository;
    }

    @Override
    public void initialize(ExamRatedForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long examRatedId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<ExamRated> examRated = examRatedRepository.findById(examRatedId);

        if (!examRated.isPresent()) {
            fieldMessagesList.add(new FieldMessage("examRatedId", "ExamRated informed doesn't exists."));
        } else if (!examRated.get().getActive()) {
            fieldMessagesList.add(new FieldMessage("examRatedId", "ExamRated informed is inactive."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
