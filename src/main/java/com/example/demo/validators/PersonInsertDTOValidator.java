package com.example.demo.validators;

import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.dtos.PersonInsertDTO;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PersonFilter;
import com.example.demo.services.PersonService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class PersonInsertDTOValidator implements ConstraintValidator<PersonInsertDTOValidations, PersonInsertDTO> {

    private final PersonService personService;

    public PersonInsertDTOValidator(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void initialize(PersonInsertDTOValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(PersonInsertDTO personInsertDTO, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        PersonFilter personFilter = new PersonFilter()
                .filterByEmail(personInsertDTO.getEmail());

        PaginatedItemsDTO page = personService.findAll(personFilter, new PageInfo());

        if (!page.getContent().isEmpty()) {
            fieldMessagesList.add(new FieldMessage("email", "E-mail already taken."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
