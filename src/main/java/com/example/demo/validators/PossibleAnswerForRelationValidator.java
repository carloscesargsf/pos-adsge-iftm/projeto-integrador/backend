package com.example.demo.validators;

import com.example.demo.entities.PossibleAnswer;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.PossibleAnswerRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PossibleAnswerForRelationValidator implements ConstraintValidator<PossibleAnswerForRelationValidations, Long> {

    private final PossibleAnswerRepository possibleAnswerRepository;

    public PossibleAnswerForRelationValidator(PossibleAnswerRepository possibleAnswerRepository) {
        this.possibleAnswerRepository = possibleAnswerRepository;
    }

    @Override
    public void initialize(PossibleAnswerForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long possibleAnswerId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<PossibleAnswer> possibleAnswer = possibleAnswerRepository.findById(possibleAnswerId);

        if (!possibleAnswer.isPresent()) {
            fieldMessagesList.add(new FieldMessage("possibleAnswerId", "PossibleAnswer informed doesn't exists."));
        } else if (!possibleAnswer.get().getActive()) {
            fieldMessagesList.add(new FieldMessage("possibleAnswerId", "PossibleAnswer informed is inactive."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
