package com.example.demo.validators;

import com.example.demo.dtos.PersonRoleInsertDTO;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.PersonRoleRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonRoleInsertDTOValidator implements ConstraintValidator<PersonRoleInsertDTOValidations, PersonRoleInsertDTO> {

    private final PersonRoleRepository personRoleRepository;

    public PersonRoleInsertDTOValidator(PersonRoleRepository personRoleRepository) {
        this.personRoleRepository = personRoleRepository;
    }

    @Override
    public void initialize(PersonRoleInsertDTOValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(PersonRoleInsertDTO personRoleInsertDTO, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<PersonRole> personRole = personRoleRepository.findByPersonIdAndCompanyIdAndRole(
                personRoleInsertDTO.getPersonId(), personRoleInsertDTO.getCompanyId(), Role.OPTIONS.get(personRoleInsertDTO.getRole()));

        if (personRole.isPresent()) {
            fieldMessagesList.add(new FieldMessage("personId", "Role already defined for this company."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
