package com.example.demo.validators;

import com.example.demo.entities.Person;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.repositories.PersonRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonForRelationValidator implements ConstraintValidator<PersonForRelationValidations, Long> {

    private final PersonRepository personRepository;

    public PersonForRelationValidator(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void initialize(PersonForRelationValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(Long personId, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        Optional<Person> person = personRepository.findById(personId);

        if (!person.isPresent()) {
            fieldMessagesList.add(new FieldMessage("personId", "Person informed doesn't exists."));
        } else {
            if (!person.get().getActive()) {
                fieldMessagesList.add(new FieldMessage("personId", "Person informed is inactive."));
            }
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
