package com.example.demo.validators;

import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.dtos.PersonUpdateDTO;
import com.example.demo.dtos.PersonViewItemDTO;
import com.example.demo.exceptions.info.FieldMessage;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PersonFilter;
import com.example.demo.services.PersonService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class PersonUpdateDTOValidator implements ConstraintValidator<PersonUpdateDTOValidations, PersonUpdateDTO> {

    private final PersonService personService;

    public PersonUpdateDTOValidator(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void initialize(PersonUpdateDTOValidations constraintAnnotation) {
        // $COVERAGE-IGNORE$
    }

    @Override
    public boolean isValid(PersonUpdateDTO personUpdateDTO, ConstraintValidatorContext constraintValidatorContext) {
        List<FieldMessage> fieldMessagesList = new ArrayList<>();

        PersonFilter personFilter = new PersonFilter()
                .filterByEmail(personUpdateDTO.getEmail());

        PaginatedItemsDTO page = personService.findAll(personFilter, new PageInfo());

        if (!page.getContent().isEmpty()
                && !((PersonViewItemDTO) page.getContent().get(0)).getId().equals(personUpdateDTO.getId())) {
            fieldMessagesList.add(new FieldMessage("email", "E-mail already taken."));
        }

        for (FieldMessage e : fieldMessagesList) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return fieldMessagesList.isEmpty();
    }

}
