package com.example.demo.security.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.entities.Person;
import com.example.demo.repositories.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.example.demo.constants.ClassAttributes.PERSON_ID;
import static com.example.demo.constants.ExceptionMessages.BAD_CREDENTIALS;

@Component
public class JWTUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JWTUtil.class);

    private final String jwtSecret;

    private final Long jwtExpiration;

    private final String jwtIssuer;

    private final String jwtPrefix;

    private final JWTVerifier jwtVerifier;

    private final PersonRepository personRepository;

    public JWTUtil(@Value("${jwt.secret}") String jwtSecret,
                   @Value("${jwt.expiration}") Long jwtExpiration,
                   @Value("${jwt.issuer}") String jwtIssuer,
                   @Value("${jwt.prefix}") String jwtPrefix, PersonRepository personRepository) {
        this.jwtSecret = jwtSecret;
        this.jwtExpiration = jwtExpiration;
        this.jwtIssuer = jwtIssuer;
        this.jwtPrefix = jwtPrefix;
        jwtVerifier = JWT.require(getAlgorithm()).withIssuer(jwtIssuer).build();
        this.personRepository = personRepository;
    }

    public String generateToken(Long personId) {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new AccessDeniedException(BAD_CREDENTIALS));

        Date now = new Date();

        return JWT.create()
                .withSubject(person.getEmail())
                .withIssuer(jwtIssuer)
                .withIssuedAt(now)
                .withExpiresAt(new Date(now.getTime() + jwtExpiration))
                .withClaim(PERSON_ID, person.getId())
                .sign(Algorithm.HMAC512(jwtSecret.getBytes()));
    }

    public boolean isValid(String token) {
        DecodedJWT decodedJWT = getDecodedJWT(token);

        if (decodedJWT != null) {
            String username = decodedJWT.getSubject();
            Date expirationDate = decodedJWT.getExpiresAt();
            Date now = new Date();

            if (username != null && expirationDate != null && now.before(expirationDate)) {
                return true;
            }
        }

        return false;
    }

    public String getUsername(String token) {
        return getDecodedJWT(token).getSubject();
    }

    private DecodedJWT getDecodedJWT(String token) {
        try {
            return jwtVerifier.verify(token);
        } catch (Exception e) {
            return null;
        }
    }

    private Algorithm getAlgorithm() {
        return Algorithm.HMAC512(jwtSecret);
    }


}
