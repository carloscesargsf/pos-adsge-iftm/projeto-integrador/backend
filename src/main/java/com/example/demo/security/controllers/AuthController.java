package com.example.demo.security.controllers;

import com.example.demo.dtos.PersonViewItemWithTokenDTO;
import com.example.demo.security.CredentialsDTO;
import com.example.demo.security.services.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public ResponseEntity<PersonViewItemWithTokenDTO> login(@RequestBody CredentialsDTO credentialsDTO) {
        return ResponseEntity.ok(authService.authenticate(credentialsDTO));
    }

    @PostMapping("/refresh")
    public ResponseEntity<PersonViewItemWithTokenDTO> refresh() {
        return ResponseEntity.ok(authService.refreshToken());
    }

}
