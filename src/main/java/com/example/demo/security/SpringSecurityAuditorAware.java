package com.example.demo.security;

import com.example.demo.entities.Person;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class SpringSecurityAuditorAware implements AuditorAware<Long> {

    public Optional<Long> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }

        return authentication.getPrincipal() instanceof String
                ? Optional.of(1L)
                : Optional.ofNullable(((Person) authentication.getPrincipal()).getId());
    }

}

