package com.example.demo.security.services;

import com.example.demo.dtos.PersonViewItemWithTokenDTO;
import com.example.demo.entities.Person;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.JWTAuthorizationException;
import com.example.demo.mappers.PersonMapper;
import com.example.demo.repositories.PersonRepository;
import com.example.demo.security.CredentialsDTO;
import com.example.demo.security.utils.JWTUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

import static com.example.demo.constants.ExceptionMessages.BAD_CREDENTIALS;

@Service
public class AuthService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);

    private final AuthenticationManager authenticationManager;

    private final PersonRepository personRepository;

    private final PersonMapper personMapper;

    private final JWTUtil jwtUtil;

    private final BCryptPasswordEncoder passwordEncoder;

    public AuthService(AuthenticationManager authenticationManager, PersonRepository personRepository,
                       PersonMapper personMapper, JWTUtil jwtUtil, BCryptPasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.jwtUtil = jwtUtil;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    public PersonViewItemWithTokenDTO authenticate(CredentialsDTO credentialsDTO) {
        PersonViewItemWithTokenDTO personViewItemWithTokenDTO = null;

        try {
            personViewItemWithTokenDTO = personMapper.toPersonViewItemWithTokenDTO(
                    personRepository.findByEmail(credentialsDTO.getEmail())
                            .orElseThrow(() -> new AccessDeniedException(BAD_CREDENTIALS)));
        } catch (UsernameNotFoundException e) {

        }
        if (personViewItemWithTokenDTO != null
                && passwordEncoder.matches(credentialsDTO.getPassword(), personViewItemWithTokenDTO.getPassword())) {

            personViewItemWithTokenDTO.setToken(jwtUtil.generateToken(personViewItemWithTokenDTO.getId()));

            return personViewItemWithTokenDTO;
        }
        throw new AccessDeniedException(BAD_CREDENTIALS);

    }

    public Person authenticated() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        return personRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(() -> new JWTAuthorizationException("Access denied"));
    }

    public void validateSelfOrAdmin(Long personId) {
        Person person = authenticated();

        if (person == null || (!person.getId().equals(personId) && !person.hasRole(Role.ADMIN.getCode()))) {
            throw new JWTAuthorizationException("Access denied");
        }
    }

    public PersonViewItemWithTokenDTO refreshToken() {
        PersonViewItemWithTokenDTO personViewItemWithTokenDTO =
                personMapper.toPersonViewItemWithTokenDTO(authenticated());

        generateTokenDTO(personViewItemWithTokenDTO);

        return personViewItemWithTokenDTO;
    }

    public void sendNewPassword(String email) {
        Person person = personRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("Email not found"));

        String newPassword = newPassword();

        person.setPassword(passwordEncoder.encode(newPassword));
        personRepository.save(person);

        LOGGER.info("New password: " + newPassword);
    }

    private void generateTokenDTO(PersonViewItemWithTokenDTO personViewItemWithTokenDTO) {
        personViewItemWithTokenDTO.setToken(jwtUtil.generateToken(personViewItemWithTokenDTO.getId()));
    }

    private String newPassword() {
        char[] vector = new char[10];

        for (int i = 0; i < 10; i++) {
            vector[i] = randomChar();
        }

        return new String(vector);
    }

    private char randomChar() {
        Random random = new Random();

        int option = random.nextInt(3);
        if (option == 0) {
            return (char) (random.nextInt(10) + 48);
        } else if (option == 1) {
            return (char) (random.nextInt(26) + 65);
        } else {
            return (char) (random.nextInt(26) + 97);
        }
    }

}
