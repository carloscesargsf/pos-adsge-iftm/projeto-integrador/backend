package com.example.demo.services;

import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.dtos.QuestionTypeViewItemDTO;
import com.example.demo.entities.QuestionType;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.QuestionTypeFilter;
import com.example.demo.mappers.QuestionTypeMapper;
import com.example.demo.repositories.QuestionTypeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.demo.constants.ExceptionMessages.QUESTION_TYPE_WITH_ID_NOT_FOUND;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class QuestionTypeService extends BaseService {

    private final QuestionTypeRepository questionTypeRepository;

    private final QuestionTypeMapper questionTypeMapper;

    public QuestionTypeService(QuestionTypeRepository questionTypeRepository, QuestionTypeMapper questionTypeMapper) {
        this.questionTypeRepository = questionTypeRepository;
        this.questionTypeMapper = questionTypeMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(QuestionTypeFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<QuestionType> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? questionTypeRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(questionTypeRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, questionTypeMapper::toQuestionTypeViewItemDTO);
    }

    @Transactional(readOnly = true)
    public QuestionTypeViewItemDTO findById(Long id) {
        return questionTypeRepository.findById(id)
                .map(questionTypeMapper::toQuestionTypeViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(QUESTION_TYPE_WITH_ID_NOT_FOUND, id));
    }

}
