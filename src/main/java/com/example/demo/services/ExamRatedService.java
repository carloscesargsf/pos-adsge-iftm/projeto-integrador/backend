package com.example.demo.services;

import com.example.demo.dtos.ExamRatedInsertDTO;
import com.example.demo.dtos.ExamRatedUpdateDTO;
import com.example.demo.dtos.ExamRatedViewItemDTO;
import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.entities.Exam;
import com.example.demo.entities.ExamRated;
import com.example.demo.entities.PersonRole;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.ExamRatedFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.mappers.ExamRatedMapper;
import com.example.demo.repositories.ExamRatedRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.example.demo.constants.ExceptionMessages.*;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class ExamRatedService extends BaseService {

    private final ExamRatedRepository examRatedRepository;

    private final ExamRatedMapper examRatedMapper;

    public ExamRatedService(ExamRatedRepository examRatedRepository, ExamRatedMapper examRatedMapper) {
        this.examRatedRepository = examRatedRepository;
        this.examRatedMapper = examRatedMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(ExamRatedFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<ExamRated> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? examRatedRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(examRatedRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, examRatedMapper::toExamRatedViewItemDTO);
    }

    @Transactional(readOnly = true)
    public ExamRatedViewItemDTO findById(Long id) {
        return examRatedRepository.findById(id)
                .map(examRatedMapper::toExamRatedViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_RATED_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public ExamRatedViewItemDTO create(ExamRatedInsertDTO dto) {
        return create(examRatedMapper.fromExamRatedInsertDTO(dto));
    }

    @Transactional
    public ExamRatedViewItemDTO create(ExamRated examRated) {
        return examRatedMapper.toExamRatedViewItemDTO(examRatedRepository.save(examRated));
    }

    @Transactional
    public void update(Long id, ExamRatedUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        ExamRated entityDB = examRatedRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_RATED_WITH_ID_NOT_FOUND, id));

        if (dto.getExamId() != null) {
            entityDB.setExam(new Exam(dto.getExamId()));
        }
        if (dto.getRatedId() != null) {
            entityDB.setRated(new PersonRole(dto.getRatedId()));
        }

        examRatedRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        ExamRated entityDB = examRatedRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_RATED_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        examRatedRepository.save(entityDB);
    }

    public Optional<ExamRated> findByExamIdAndRatedId(Long examId, Long ratedId) {
        return examRatedRepository.findByExamIdAndRatedId(examId, ratedId);
    }

}
