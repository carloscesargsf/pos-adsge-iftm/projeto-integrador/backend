package com.example.demo.services;

import com.example.demo.dtos.*;
import com.example.demo.entities.Category;
import com.example.demo.entities.PossibleAnswer;
import com.example.demo.entities.Question;
import com.example.demo.entities.QuestionType;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.QuestionFilter;
import com.example.demo.mappers.QuestionMapper;
import com.example.demo.repositories.QuestionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.demo.constants.ExceptionMessages.*;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class QuestionService extends BaseService {

    private final QuestionRepository questionRepository;

    private final QuestionMapper questionMapper;

    private final PossibleAnswerService possibleAnswerService;

    public QuestionService(QuestionRepository questionRepository, QuestionMapper questionMapper,
                           PossibleAnswerService possibleAnswerService) {
        this.questionRepository = questionRepository;
        this.questionMapper = questionMapper;
        this.possibleAnswerService = possibleAnswerService;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(QuestionFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<Question> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? questionRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(questionRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, questionMapper::toQuestionViewItemDTO);
    }

    @Transactional(readOnly = true)
    public QuestionViewItemDTO findById(Long id) {
        return questionRepository.findById(id)
                .map(questionMapper::toQuestionViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(QUESTION_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public QuestionViewItemDTO create(QuestionInsertDTO dto) {
        Question question = questionMapper.fromQuestionInsertDTO(dto);

        questionRepository.save(question);

        question.getPossibleAnswers().forEach(possibleAnswer -> possibleAnswerService.create(possibleAnswer));

        return questionMapper.toQuestionViewItemDTO(question);
    }

    @Transactional
    public void update(Long id, QuestionUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        Question entityDB = questionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(QUESTION_WITH_ID_NOT_FOUND, id));

        if (dto.getQuestionTypeId() != null) {
            entityDB.setQuestionType(new QuestionType(dto.getQuestionTypeId()));
        }
        if (dto.getCategoryId() != null) {
            entityDB.setCategory(new Category(dto.getCategoryId()));
        }
        entityDB.setTitle(dto.getTitle());
        entityDB.getPossibleAnswers().forEach(pa -> {
            QuestionPossibleAnswerDTO questionPossibleAnswerDTO = new QuestionPossibleAnswerDTO();
            questionPossibleAnswerDTO.setDescription(pa.getDescription());
            questionPossibleAnswerDTO.setCorrect(pa.getCorrect());

            if (!dto.getPossibleAnswers().contains(questionPossibleAnswerDTO)) {
                possibleAnswerService.inactivate(pa.getId());
            }
        });
        dto.getPossibleAnswers().forEach(questionPossibleAnswerDTO -> {
            PossibleAnswer possibleAnswer = new PossibleAnswer();
            possibleAnswer.setDescription(questionPossibleAnswerDTO.getDescription());
            possibleAnswer.setQuestion(entityDB);
            possibleAnswer.setCorrect(questionPossibleAnswerDTO.getCorrect());
            possibleAnswer.setActive(Boolean.TRUE);
            if (!entityDB.getPossibleAnswers().contains(possibleAnswer)) {
                entityDB.getPossibleAnswers().add(possibleAnswer);
                possibleAnswerService.create(possibleAnswer);
            } else {
                PossibleAnswer possible = entityDB.getPossibleAnswers()
                        .stream()
                        .filter(pa -> pa.equals(possibleAnswer))
                        .findFirst()
                        .orElseThrow(() -> new IllegalArgumentException(""));
                possible.setCorrect(possibleAnswer.getCorrect());
                possibleAnswerService.update(possible.getId(), possible);
            }
        });

        questionRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        Question entityDB = questionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(QUESTION_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        questionRepository.save(entityDB);
    }

}
