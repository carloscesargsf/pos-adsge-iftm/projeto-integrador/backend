package com.example.demo.services;

import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.dtos.PersonInsertDTO;
import com.example.demo.dtos.PersonUpdateDTO;
import com.example.demo.dtos.PersonViewItemDTO;
import com.example.demo.entities.Person;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Gender;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PersonFilter;
import com.example.demo.mappers.PersonMapper;
import com.example.demo.repositories.PersonRepository;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.demo.constants.ExceptionMessages.*;
import static com.example.demo.utils.UserUtils.getPrincipal;

@Service
public class PersonService extends BaseService implements UserDetailsService {

    private final PersonRepository personRepository;

    private final PersonMapper personMapper;

    private final PasswordEncoder passwordEncoder;

    private final PersonRoleService personRoleService;

    public PersonService(PersonRepository personRepository, PersonMapper personMapper,
                         PasswordEncoder passwordEncoder, PersonRoleService personRoleService) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.passwordEncoder = passwordEncoder;
        this.personRoleService = personRoleService;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(PersonFilter filters, PageInfo pageInfo) {
        filters.filterByCompanyId(filters.getCompanyId()
                .orElse(getPrincipal().getPersonRoles().get(0).getCompany().getId()));

        return new PaginatedItemsDTO<>(personRepository.findAll(filters, Sort.by("name").ascending())
                .stream()
                .map(personMapper::toPersonViewItemDTO)
                .collect(Collectors.toList()));
    }

    @Transactional(readOnly = true)
    public PersonViewItemDTO findById(Long id) {
        return personRepository.findById(id)
                .map(personMapper::toPersonViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(PERSON_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public PersonViewItemDTO create(PersonInsertDTO dto) {
        return create(personMapper.fromPersonInsertDTO(dto));
    }

    @Transactional
    public PersonViewItemDTO create(Person person) {
        person.setPassword(passwordEncoder.encode(person.getPassword()));

        personRepository.save(person);

        person.getPersonRoles().forEach(personRole -> personRoleService.create(personRole));

        return personMapper.toPersonViewItemDTO(person);
    }

    @Transactional
    public void update(Long id, PersonUpdateDTO dto) {
        if (getPrincipal().getPersonRoles() == null || getPrincipal().getPersonRoles().get(0) == null
                || getPrincipal().getPersonRoles().get(0).getCompany() == null) {
            throw new IllegalArgumentException("Logged user is not associated with a company!");
        }

        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        Person entityDB = personRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(PERSON_WITH_ID_NOT_FOUND, id));

        entityDB.setName(dto.getName());
        entityDB.setCpf(dto.getCpf());
        entityDB.setGender(Gender.OPTIONS.get(dto.getGender()));
        entityDB.setEmail(dto.getEmail());
        if (dto.getPassword() != null) {
            entityDB.setPassword(passwordEncoder.encode(dto.getPassword()));
        }

        entityDB.getPersonRoles().forEach(pr -> {
            if (!dto.getRoles().contains(pr.getRole().getCode())) {
                personRoleService.inactivate(pr.getId());
            }
        });
        dto.getRoles().forEach(role -> {
            Optional<PersonRole> personRole = personRoleService.findByPersonIdAndCompanyIdAndRole(entityDB.getId(),
                    getPrincipal().getPersonRoles().get(0).getCompany().getId(), Role.OPTIONS.get(role));
            if (!personRole.isPresent()) {
                PersonRole pr = new PersonRole();

                pr.setPerson(entityDB);
                pr.setCompany(getPrincipal().getPersonRoles().get(0).getCompany());
                pr.setRole(Role.OPTIONS.get(role));
                pr.setActive(Boolean.TRUE);

                entityDB.getPersonRoles().add(pr);
                personRoleService.create(pr);
            } else {
                personRoleService.activate(personRole.get().getId());
            }
        });

        personRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return personRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        Person entityDB = personRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(PERSON_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        personRepository.save(entityDB);
    }

}
