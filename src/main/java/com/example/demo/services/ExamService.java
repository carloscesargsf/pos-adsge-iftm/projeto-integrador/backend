package com.example.demo.services;

import com.example.demo.dtos.ExamInsertDTO;
import com.example.demo.dtos.ExamUpdateDTO;
import com.example.demo.dtos.ExamViewItemDTO;
import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.entities.*;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.IllegalArgumentException;
import com.example.demo.filters.ExamFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.mappers.ExamMapper;
import com.example.demo.repositories.ExamRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.demo.constants.ExceptionMessages.*;
import static com.example.demo.utils.UserUtils.getPrincipal;

@Service
public class ExamService extends BaseService {

    private final ExamRepository examRepository;

    private final ExamMapper examMapper;

    private final ExamQuestionService examQuestionService;

    private final ExamRatedService examRatedService;

    public ExamService(ExamRepository examRepository, ExamMapper examMapper, ExamQuestionService examQuestionService,
                       ExamRatedService examRatedService) {
        this.examRepository = examRepository;
        this.examMapper = examMapper;
        this.examQuestionService = examQuestionService;
        this.examRatedService = examRatedService;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(ExamFilter filters, PageInfo pageInfo) {
        filters.filterByCompanyId(filters.getCompanyId()
                .orElse(getPrincipal().getPersonRoles().get(0).getCompany().getId()));

        return new PaginatedItemsDTO<>(examRepository.findAll(filters, Sort.by("beginDate", "name").ascending())
                .stream()
                .map(examMapper::toExamViewItemDTO)
                .collect(Collectors.toList()));
    }

    @Transactional(readOnly = true)
    public ExamViewItemDTO findById(Long id) {
        return examRepository.findById(id)
                .map(examMapper::toExamViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public ExamViewItemDTO create(ExamInsertDTO dto) {
        Exam exam = examMapper.fromExamInsertDTO(dto);
        exam.setEvaluator(getPrincipal().getPersonRoles()
                .stream()
                .filter(personRole -> personRole.getRole().equals(Role.EVALUATOR))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(USER_DOESNT_HAVE_THIS_PERMISSION, Role.EVALUATOR.getLabel())));

        examRepository.save(exam);

        exam.getExamQuestions().forEach(examQuestion -> examQuestionService.create(examQuestion));

        return examMapper.toExamViewItemDTO(exam);
    }

    @Transactional
    public void update(Long id, ExamUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        Exam entityDB = examRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_WITH_ID_NOT_FOUND, id));

        entityDB.setName(dto.getName());
        entityDB.setDescription(dto.getDescription());
        entityDB.setBeginDate(dto.getBeginDate());
        entityDB.setEndDate(dto.getEndDate());
        entityDB.setGrade(dto.getGrade());
        entityDB.getExamQuestions().forEach(examQuestion -> {
            if (!dto.getQuestionIds().contains(examQuestion.getQuestion().getId())) {
                examQuestionService.inactivate(examQuestion.getId());
            }
        });
        dto.getQuestionIds().forEach(questionId -> {
            Optional<ExamQuestion> examQuestion = examQuestionService.findByExamIdAndQuestionId(entityDB.getId(), questionId);
            if (!examQuestion.isPresent()) {
                ExamQuestion eq = new ExamQuestion();
                eq.setExam(entityDB);
                eq.setQuestion(new Question(questionId));
                eq.setActive(Boolean.TRUE);

                entityDB.getExamQuestions().add(eq);
                examQuestionService.create(eq);
            } else {
                examQuestionService.activate(examQuestion.get().getId());
            }
        });
        entityDB.getExamRated().forEach(examRated -> {
            if (!dto.getRatedIds().contains(examRated.getRated().getId())) {
                examRatedService.inactivate(examRated.getId());
            }
        });
        dto.getRatedIds().forEach(ratedId -> {
            Optional<ExamRated> examRated = examRatedService.findByExamIdAndRatedId(entityDB.getId(), ratedId);
            if (!examRated.isPresent()) {
                ExamRated er = new ExamRated();
                er.setExam(entityDB);
                er.setRated(new PersonRole(ratedId));
                er.setActive(Boolean.TRUE);

                entityDB.getExamRated().add(er);
                examRatedService.create(er);
            } else {
                examRatedService.activate(examRated.get().getId());
            }
        });

        examRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        Exam entityDB = examRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        examRepository.save(entityDB);
    }

}
