package com.example.demo.services;

import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.dtos.PossibleAnswerInsertDTO;
import com.example.demo.dtos.PossibleAnswerUpdateDTO;
import com.example.demo.dtos.PossibleAnswerViewItemDTO;
import com.example.demo.entities.PossibleAnswer;
import com.example.demo.entities.Question;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PossibleAnswerFilter;
import com.example.demo.mappers.PossibleAnswerMapper;
import com.example.demo.repositories.PossibleAnswerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.demo.constants.ExceptionMessages.*;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class PossibleAnswerService extends BaseService {

    private final PossibleAnswerRepository possibleAnswerRepository;

    private final PossibleAnswerMapper possibleAnswerMapper;

    public PossibleAnswerService(PossibleAnswerRepository possibleAnswerRepository, PossibleAnswerMapper possibleAnswerMapper) {
        this.possibleAnswerRepository = possibleAnswerRepository;
        this.possibleAnswerMapper = possibleAnswerMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(PossibleAnswerFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<PossibleAnswer> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? possibleAnswerRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(possibleAnswerRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, possibleAnswerMapper::toPossibleAnswerViewItemDTO);
    }

    @Transactional(readOnly = true)
    public PossibleAnswerViewItemDTO findById(Long id) {
        return possibleAnswerRepository.findById(id)
                .map(possibleAnswerMapper::toPossibleAnswerViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(POSSIBLE_ANSWER_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public PossibleAnswerViewItemDTO create(PossibleAnswerInsertDTO dto) {
        return create(possibleAnswerMapper.fromPossibleAnswerInsertDTO(dto));
    }

    @Transactional
    public PossibleAnswerViewItemDTO create(PossibleAnswer possibleAnswer) {
        return possibleAnswerMapper.toPossibleAnswerViewItemDTO(possibleAnswerRepository.save(possibleAnswer));
    }

    @Transactional
    public void update(Long id, PossibleAnswerUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        PossibleAnswer entityDB = possibleAnswerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(POSSIBLE_ANSWER_WITH_ID_NOT_FOUND, id));

        if (dto.getQuestionId() != null) {
            entityDB.setQuestion(new Question(dto.getQuestionId()));
        }
        entityDB.setDescription(dto.getDescription());
        entityDB.setCorrect(dto.getCorrect());

        update(entityDB.getId(), entityDB);
    }

    @Transactional
    public void update(Long id, PossibleAnswer possibleAnswer) {
        if (id != possibleAnswer.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        possibleAnswerRepository.save(possibleAnswer);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        PossibleAnswer entityDB = possibleAnswerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(POSSIBLE_ANSWER_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        possibleAnswerRepository.save(entityDB);
    }

}
