package com.example.demo.services;

import com.example.demo.dtos.AnswerInsertDTO;
import com.example.demo.dtos.AnswerUpdateDTO;
import com.example.demo.dtos.AnswerViewItemDTO;
import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.entities.Answer;
import com.example.demo.entities.ExamQuestion;
import com.example.demo.entities.ExamRated;
import com.example.demo.entities.PossibleAnswer;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.AnswerFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.mappers.AnswerMapper;
import com.example.demo.repositories.AnswerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.demo.constants.ExceptionMessages.*;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class AnswerService extends BaseService {

    private final AnswerRepository answerRepository;

    private final AnswerMapper answerMapper;

    public AnswerService(AnswerRepository answerRepository, AnswerMapper answerMapper) {
        this.answerRepository = answerRepository;
        this.answerMapper = answerMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(AnswerFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<Answer> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? answerRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(answerRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, answerMapper::toAnswerViewItemDTO);
    }

    @Transactional(readOnly = true)
    public AnswerViewItemDTO findById(Long id) {
        return answerRepository.findById(id)
                .map(answerMapper::toAnswerViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(ANSWER_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public AnswerViewItemDTO create(AnswerInsertDTO dto) {
        return answerMapper.toAnswerViewItemDTO(answerRepository.save(answerMapper.fromAnswerInsertDTO(dto)));
    }

    @Transactional
    public void update(Long id, AnswerUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        Answer entityDB = answerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(ANSWER_WITH_ID_NOT_FOUND, id));

        if (dto.getExamRatedId() != null) {
            entityDB.setExamRated(new ExamRated(dto.getExamRatedId()));
        }
        if (dto.getExamQuestionId() != null) {
            entityDB.setExamQuestion(new ExamQuestion(dto.getExamQuestionId()));
        }
        if (dto.getPossibleAnswerId() != null) {
            entityDB.setPossibleAnswer(new PossibleAnswer(dto.getPossibleAnswerId()));
        }

        answerRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        Answer entityDB = answerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(ANSWER_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        answerRepository.save(entityDB);
    }

}
