package com.example.demo.services;

import com.example.demo.dtos.*;
import com.example.demo.entities.Category;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.CategoryFilter;
import com.example.demo.mappers.CategoryMapper;
import com.example.demo.repositories.CategoryRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.example.demo.constants.ExceptionMessages.*;

@Service
public class CategoryService extends BaseService {

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    public CategoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(CategoryFilter filters) {
        List<Category> categories = categoryRepository.findAll(filters, Sort.by("name").ascending());

        List<CategoryListDTO> categoriesList = new ArrayList<>();

        categories.forEach(category -> addCategoryToList(category, "", categoriesList));

        return new PaginatedItemsDTO<>(categoriesList);
    }

    private void addCategoryToList(Category category, String parentCategoryName, List<CategoryListDTO> categoriesList) {
        CategoryListDTO categoryListDTO = new CategoryListDTO();
        categoryListDTO.setId(category.getId());
        categoryListDTO.setName(parentCategoryName.concat(category.getName()));
        categoryListDTO.setDescription(category.getDescription());
        categoryListDTO.setActive(category.getActive());

        categoriesList.add(categoryListDTO);

        category.getChildCategories().forEach(childCategory -> addCategoryToList(childCategory,
                categoryListDTO.getName().concat("::"), categoriesList));
    }

    @Transactional(readOnly = true)
    public CategoryViewItemDTO findById(Long id) {
        return categoryRepository.findById(id)
                .map(categoryMapper::toCategoryViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(CATEGORY_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public CategoryViewItemDTO create(CategoryInsertDTO dto) {
        return categoryMapper.toCategoryViewItemDTO(categoryRepository.save(categoryMapper.fromCategoryInsertDTO(dto)));
    }

    @Transactional
    public void update(Long id, CategoryUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        Category entityDB = categoryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(CATEGORY_WITH_ID_NOT_FOUND, id));

        entityDB.setName(dto.getName());
        entityDB.setDescription(dto.getDescription());
        entityDB.setParentCategory(new Category(dto.getParentCategoryId()));

        categoryRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        Category entityDB = categoryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(CATEGORY_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        categoryRepository.save(entityDB);
    }

}
