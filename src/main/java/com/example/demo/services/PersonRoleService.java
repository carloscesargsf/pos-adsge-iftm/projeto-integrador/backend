package com.example.demo.services;

import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.dtos.PersonRoleInsertDTO;
import com.example.demo.dtos.PersonRoleUpdateDTO;
import com.example.demo.dtos.PersonRoleViewItemDTO;
import com.example.demo.entities.Company;
import com.example.demo.entities.Person;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PersonRoleFilter;
import com.example.demo.mappers.PersonRoleMapper;
import com.example.demo.repositories.PersonRoleRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.demo.constants.ExceptionMessages.*;
import static com.example.demo.utils.UserUtils.getPrincipal;

@Service
public class PersonRoleService extends BaseService {

    private final PersonRoleRepository personRoleRepository;

    private final PersonRoleMapper personRoleMapper;

    public PersonRoleService(PersonRoleRepository personRoleRepository, PersonRoleMapper personRoleMapper) {
        this.personRoleRepository = personRoleRepository;
        this.personRoleMapper = personRoleMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(PersonRoleFilter filters, PageInfo pageInfo) {
        filters.filterByCompanyId(filters.getCompanyId()
                .orElse(getPrincipal().getPersonRoles().get(0).getCompany().getId()));

        return new PaginatedItemsDTO<>(personRoleRepository.findAll(filters, Sort.by("person.name").ascending())
                .stream()
                .map(personRoleMapper::toPersonRoleViewItemDTO)
                .collect(Collectors.toList()));
    }

    @Transactional(readOnly = true)
    public PersonRoleViewItemDTO findById(Long id) {
        return personRoleRepository.findById(id)
                .map(personRoleMapper::toPersonRoleViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(PERSON_ROLE_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public PersonRoleViewItemDTO create(PersonRoleInsertDTO dto) {
        PersonRole personRole = personRoleMapper.fromPersonRoleInsertDTO(dto);

        return create(personRole);
    }

    @Transactional
    public PersonRoleViewItemDTO create(PersonRole personRole) {
        return personRoleMapper.toPersonRoleViewItemDTO(personRoleRepository.save(personRole));
    }

    @Transactional
    public void update(Long id, PersonRoleUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        PersonRole entityDB = personRoleRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(PERSON_ROLE_WITH_ID_NOT_FOUND, id));

        if (dto.getPersonId() != null) {
            entityDB.setPerson(new Person(dto.getPersonId()));
        }
        if (dto.getCompanyId() != null) {
            entityDB.setCompany(new Company(dto.getCompanyId()));
        }
        if (dto.getRole() != null) {
            entityDB.setRole(Role.OPTIONS.get(dto.getRole()));
        }

        personRoleRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        PersonRole entityDB = personRoleRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(PERSON_ROLE_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        personRoleRepository.save(entityDB);
    }

    public Optional<PersonRole> findByPersonIdAndCompanyIdAndRole(Long personId, Long companyId, Role role) {
        return personRoleRepository.findByPersonIdAndCompanyIdAndRole(personId, companyId, role);
    }

}
