package com.example.demo.services;

import com.example.demo.dtos.CompanyInsertDTO;
import com.example.demo.dtos.CompanyUpdateDTO;
import com.example.demo.dtos.CompanyViewItemDTO;
import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.entities.Company;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.CompanyFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.mappers.CompanyMapper;
import com.example.demo.repositories.CompanyRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.demo.constants.ExceptionMessages.*;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class CompanyService extends BaseService {

    private final CompanyRepository companyRepository;

    private final CompanyMapper companyMapper;

    private final PersonService personService;

    public CompanyService(CompanyRepository companyRepository, CompanyMapper companyMapper,
                          PersonService personService) {
        this.companyRepository = companyRepository;
        this.companyMapper = companyMapper;
        this.personService = personService;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(CompanyFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<Company> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? companyRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(companyRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, companyMapper::toCompanyViewItemDTO);
    }

    @Transactional(readOnly = true)
    public CompanyViewItemDTO findById(Long id) {
        return companyRepository.findById(id)
                .map(companyMapper::toCompanyViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(COMPANY_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public CompanyViewItemDTO create(CompanyInsertDTO dto) {
        Company company = companyMapper.fromCompanyInsertDTO(dto);

        companyRepository.save(company);

        company.getPeople().forEach(personRole -> personService.create(personRole.getPerson()));

        return companyMapper.toCompanyViewItemDTO(company);
    }

    @Transactional
    public void update(Long id, CompanyUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        Company entityDB = companyRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(COMPANY_WITH_ID_NOT_FOUND, id));

        entityDB.setCompanyName(dto.getCompanyName());
        entityDB.setTradingName(dto.getTradingName());
        entityDB.setCnpj(dto.getCnpj());

        companyRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        Company entityDB = companyRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(COMPANY_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        companyRepository.save(entityDB);
    }

}
