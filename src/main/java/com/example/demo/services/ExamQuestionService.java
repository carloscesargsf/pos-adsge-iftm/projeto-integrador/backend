package com.example.demo.services;

import com.example.demo.dtos.ExamQuestionInsertDTO;
import com.example.demo.dtos.ExamQuestionUpdateDTO;
import com.example.demo.dtos.ExamQuestionViewItemDTO;
import com.example.demo.dtos.PaginatedItemsDTO;
import com.example.demo.entities.Exam;
import com.example.demo.entities.ExamQuestion;
import com.example.demo.entities.Question;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.filters.ExamQuestionFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.mappers.ExamQuestionMapper;
import com.example.demo.repositories.ExamQuestionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.example.demo.constants.ExceptionMessages.*;
import static org.springframework.data.domain.Sort.unsorted;

@Service
public class ExamQuestionService extends BaseService {

    private final ExamQuestionRepository examQuestionRepository;

    private final ExamQuestionMapper examQuestionMapper;

    public ExamQuestionService(ExamQuestionRepository examQuestionRepository, ExamQuestionMapper examQuestionMapper) {
        this.examQuestionRepository = examQuestionRepository;
        this.examQuestionMapper = examQuestionMapper;
    }

    @Transactional(readOnly = true)
    public PaginatedItemsDTO findAll(ExamQuestionFilter filters, PageInfo pageInfo) {
        Sort sort = pageInfo.getSortBy().isPresent()
                ? Sort.by(pageInfo.getSortBy().get())
                : unsorted();
        sort = pageInfo.getAscending() == null || pageInfo.getAscending()
                ? sort.ascending()
                : sort.descending();

        Page<ExamQuestion> page = (pageInfo.getPage().isPresent() && pageInfo.getPageSize().isPresent() && pageInfo.getPageSize().get() > 0)
                ? examQuestionRepository.findAll(filters, PageRequest.of(pageInfo.getPage().get(), pageInfo.getPageSize().get(), sort))
                : new PageImpl<>(examQuestionRepository.findAll(filters, sort));

        return new PaginatedItemsDTO<>(page, pageInfo, examQuestionMapper::toExamQuestionViewItemDTO);
    }

    @Transactional(readOnly = true)
    public ExamQuestionViewItemDTO findById(Long id) {
        return examQuestionRepository.findById(id)
                .map(examQuestionMapper::toExamQuestionViewItemDTO)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_QUESTION_WITH_ID_NOT_FOUND, id));
    }

    @Transactional
    public ExamQuestionViewItemDTO create(ExamQuestionInsertDTO dto) {
        return create(examQuestionMapper.fromExamQuestionInsertDTO(dto));
    }

    @Transactional
    public ExamQuestionViewItemDTO create(ExamQuestion examQuestion) {
        return examQuestionMapper.toExamQuestionViewItemDTO(examQuestionRepository.save(examQuestion));
    }

    @Transactional
    public void update(Long id, ExamQuestionUpdateDTO dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException(INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT);
        }

        ExamQuestion entityDB = examQuestionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_QUESTION_WITH_ID_NOT_FOUND, id));

        if (dto.getExamId() != null) {
            entityDB.setExam(new Exam(dto.getExamId()));
        }
        if (dto.getQuestionId() != null) {
            entityDB.setQuestion(new Question(dto.getQuestionId()));
        }

        examQuestionRepository.save(entityDB);
    }

    @Transactional
    public void activate(Long id) {
        activateInactivate(id, Boolean.TRUE);
    }

    @Transactional
    public void inactivate(Long id) {
        activateInactivate(id, Boolean.FALSE);
    }

    private void activateInactivate(Long id, Boolean active) {
        if (id == null) {
            throw new IllegalArgumentException(ID_IS_MANDATORY);
        }

        ExamQuestion entityDB = examQuestionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(EXAM_QUESTION_WITH_ID_NOT_FOUND, id));
        entityDB.setActive(active);

        examQuestionRepository.save(entityDB);
    }

    public Optional<ExamQuestion> findByExamIdAndQuestionId(Long examId, Long questionId) {
        return examQuestionRepository.findByExamIdAndQuestionId(examId, questionId);
    }

}
