package com.example.demo.mappers;

import com.example.demo.dtos.ExamQuestionInsertDTO;
import com.example.demo.dtos.ExamQuestionViewItemDTO;
import com.example.demo.entities.Exam;
import com.example.demo.entities.ExamQuestion;
import com.example.demo.entities.Question;
import org.springframework.stereotype.Component;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class ExamQuestionMapper {

    private final QuestionMapper questionMapper;

    public ExamQuestionMapper(QuestionMapper questionMapper) {
        this.questionMapper = questionMapper;
    }

    public ExamQuestionViewItemDTO toExamQuestionViewItemDTO(ExamQuestion entity) {
        checkIfNull(entity);

        ExamQuestionViewItemDTO dto = new ExamQuestionViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getExam() != null) {
            dto.setExamId(entity.getExam().getId());
        }
        if (entity.getQuestion() != null) {
            dto.setQuestion(questionMapper.toQuestionViewItemDTO(entity.getQuestion()));
        }
        dto.setActive(entity.getActive());

        return dto;
    }

    public ExamQuestion fromExamQuestionInsertDTO(ExamQuestionInsertDTO dto) {
        checkIfNull(dto);

        ExamQuestion entity = new ExamQuestion();

        if (dto.getExamId() != null) {
            entity.setExam(new Exam(dto.getExamId()));
        }
        if (dto.getQuestionId() != null) {
            entity.setQuestion(new Question(dto.getQuestionId()));
        }

        return entity;
    }

}
