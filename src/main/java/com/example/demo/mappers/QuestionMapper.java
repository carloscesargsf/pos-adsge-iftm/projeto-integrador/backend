package com.example.demo.mappers;

import com.example.demo.dtos.QuestionInsertDTO;
import com.example.demo.dtos.QuestionViewItemDTO;
import com.example.demo.entities.Category;
import com.example.demo.entities.PossibleAnswer;
import com.example.demo.entities.Question;
import com.example.demo.entities.QuestionType;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.stream.Collectors;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class QuestionMapper {

    private final PossibleAnswerMapper possibleAnswerMapper;

    public QuestionMapper(PossibleAnswerMapper possibleAnswerMapper) {
        this.possibleAnswerMapper = possibleAnswerMapper;
    }

    public QuestionViewItemDTO toQuestionViewItemDTO(Question entity) {
        checkIfNull(entity);

        QuestionViewItemDTO dto = new QuestionViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getQuestionType() != null) {
            dto.setQuestionTypeId(entity.getQuestionType().getId());
            dto.setQuestionTypeName(entity.getQuestionType().getName());
        }
        if (entity.getCategory() != null) {
            dto.setCategoryId(entity.getCategory().getId());
            dto.setCategoryName(entity.getCategory().getName());
        }
        dto.setTitle(entity.getTitle());
        dto.setActive(entity.getActive());
        dto.setPossibleAnswers(entity.getPossibleAnswers() != null
                ? entity.getPossibleAnswers()
                .stream()
                .map(e -> possibleAnswerMapper.toPossibleAnswerViewItemDTO(e))
                .collect(Collectors.toList())
                : new LinkedList<>());

        return dto;
    }

    public Question fromQuestionInsertDTO(QuestionInsertDTO dto) {
        checkIfNull(dto);

        Question entity = new Question();

        if (dto.getQuestionTypeId() != null) {
            entity.setQuestionType(new QuestionType(dto.getQuestionTypeId()));
        }
        if (dto.getCategoryId() != null) {
            entity.setCategory(new Category(dto.getCategoryId()));
        }
        entity.setTitle(dto.getTitle());
        entity.setActive(Boolean.TRUE);
        entity.setPossibleAnswers(dto.getPossibleAnswers()
                .stream()
                .map(questionPossibleAnswerDTO -> {
                    PossibleAnswer possibleAnswer = new PossibleAnswer();

                    possibleAnswer.setQuestion(entity);
                    possibleAnswer.setDescription(questionPossibleAnswerDTO.getDescription());
                    possibleAnswer.setCorrect(questionPossibleAnswerDTO.getCorrect());
                    possibleAnswer.setActive(Boolean.TRUE);

                    return possibleAnswer;
                })
                .collect(Collectors.toList()));

        return entity;
    }

}
