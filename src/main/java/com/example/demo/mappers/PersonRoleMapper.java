package com.example.demo.mappers;

import com.example.demo.dtos.PersonRoleInsertDTO;
import com.example.demo.dtos.PersonRoleViewItemDTO;
import com.example.demo.entities.Company;
import com.example.demo.entities.Person;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;
import org.springframework.stereotype.Component;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class PersonRoleMapper {

    public PersonRoleViewItemDTO toPersonRoleViewItemDTO(PersonRole entity) {
        checkIfNull(entity);

        PersonRoleViewItemDTO dto = new PersonRoleViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getPerson() != null) {
            dto.setPersonId(entity.getPerson().getId());
            dto.setPersonName(entity.getPerson().getName());
            dto.setPersonCpf(entity.getPerson().getCpf());
            if (entity.getPerson().getGender() != null) {
                dto.setPersonGender(entity.getPerson().getGender().getLabel());
            }
            dto.setPersonEmail(entity.getPerson().getEmail());
        }
        if (entity.getCompany() != null) {
            dto.setCompanyId(entity.getCompany().getId());
            dto.setCompanyName(entity.getCompany().getCompanyName());
            dto.setCompanyTradingName(entity.getCompany().getTradingName());
            dto.setCompanyCnpj(entity.getCompany().getCnpj());
        }
        if (entity.getRole() != null) {
            dto.setRole(entity.getRole().getLabel());
        }
        dto.setActive(entity.getActive());

        return dto;
    }

    public PersonRole fromPersonRoleInsertDTO(PersonRoleInsertDTO dto) {
        checkIfNull(dto);

        PersonRole entity = new PersonRole();

        if (dto.getPersonId() != null) {
            entity.setPerson(new Person(dto.getPersonId()));
        }
        if (dto.getCompanyId() != null) {
            entity.setCompany(new Company(dto.getCompanyId()));
        }
        if (dto.getRole() != null) {
            entity.setRole(Role.OPTIONS.get(dto.getRole()));
        }
        entity.setActive(Boolean.TRUE);

        return entity;
    }

}
