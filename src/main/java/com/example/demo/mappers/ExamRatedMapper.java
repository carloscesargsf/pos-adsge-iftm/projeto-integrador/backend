package com.example.demo.mappers;

import com.example.demo.dtos.ExamRatedInsertDTO;
import com.example.demo.dtos.ExamRatedViewItemDTO;
import com.example.demo.entities.Exam;
import com.example.demo.entities.ExamRated;
import com.example.demo.entities.PersonRole;
import org.springframework.stereotype.Component;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class ExamRatedMapper {

    public ExamRatedViewItemDTO toExamRatedViewItemDTO(ExamRated entity) {
        checkIfNull(entity);

        ExamRatedViewItemDTO dto = new ExamRatedViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getExam() != null) {
            dto.setExamId(entity.getExam().getId());
        }
        if (entity.getRated() != null && entity.getRated().getPerson() != null) {
            dto.setRatedId(entity.getRated().getId());
            dto.setRatedName(entity.getRated().getPerson().getName());
            if (entity.getRated().getPerson().getGender() != null) {
                dto.setRatedGender(entity.getRated().getPerson().getGender().getLabel());
            }
        }
        dto.setActive(entity.getActive());

        return dto;
    }

    public ExamRated fromExamRatedInsertDTO(ExamRatedInsertDTO dto) {
        checkIfNull(dto);

        ExamRated entity = new ExamRated();

        if (dto.getExamId() != null) {
            entity.setExam(new Exam(dto.getExamId()));
        }
        if (dto.getRatedId() != null) {
            entity.setRated(new PersonRole(dto.getRatedId()));
        }

        return entity;
    }

}
