package com.example.demo.mappers;

import com.example.demo.dtos.AnswerInsertDTO;
import com.example.demo.dtos.AnswerViewItemDTO;
import com.example.demo.entities.Answer;
import com.example.demo.entities.ExamQuestion;
import com.example.demo.entities.ExamRated;
import com.example.demo.entities.PossibleAnswer;
import org.springframework.stereotype.Component;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class AnswerMapper {

    public AnswerViewItemDTO toAnswerViewItemDTO(Answer entity) {
        checkIfNull(entity);
        checkIfNull(entity.getExamRated());
        checkIfNull(entity.getExamRated().getExam());
        checkIfNull(entity.getExamRated().getRated());
        checkIfNull(entity.getExamQuestion());
        checkIfNull(entity.getExamQuestion().getExam());
        checkIfNull(entity.getExamQuestion().getQuestion());
        checkIfNull(entity.getPossibleAnswer());

        AnswerViewItemDTO dto = new AnswerViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getExamRated() != null) {
            if (entity.getExamRated().getExam() != null) {
                dto.setExamId(entity.getExamRated().getExam().getId());
                dto.setExamName(entity.getExamRated().getExam().getName());
                dto.setExamBeginDate(entity.getExamRated().getExam().getBeginDate());
                dto.setExamEndDate(entity.getExamRated().getExam().getEndDate());
                dto.setExamGrade(entity.getExamRated().getExam().getGrade());
            }
            if (entity.getExamRated().getRated() != null && entity.getExamRated().getRated().getPerson() != null) {
                dto.setRatedId(entity.getExamRated().getRated().getId());
                dto.setRatedName(entity.getExamRated().getRated().getPerson().getName());
                if (entity.getExamRated().getRated().getPerson().getGender() != null) {
                    dto.setRatedGender(entity.getExamRated().getRated().getPerson().getGender().getLabel());
                }
            }
        }
        if (entity.getExamQuestion() != null) {
            if (entity.getExamQuestion().getExam() != null) {
                dto.setExamId(entity.getExamQuestion().getExam().getId());
                dto.setExamName(entity.getExamQuestion().getExam().getName());
                dto.setExamBeginDate(entity.getExamQuestion().getExam().getBeginDate());
                dto.setExamEndDate(entity.getExamQuestion().getExam().getEndDate());
                dto.setExamGrade(entity.getExamQuestion().getExam().getGrade());
            }
            if (entity.getExamQuestion().getQuestion() != null) {
                dto.setQuestionId(entity.getExamQuestion().getQuestion().getId());
                dto.setQuestionTitle(entity.getExamQuestion().getQuestion().getTitle());
            }
        }
        if (entity.getPossibleAnswer() != null) {
            dto.setPossibleAnswerId(entity.getPossibleAnswer().getId());
            dto.setPossibleAnswerDescription(entity.getPossibleAnswer().getDescription());
            dto.setPossibleAnswerCorrect(entity.getPossibleAnswer().getCorrect());
        }
        dto.setActive(entity.getActive());

        return dto;
    }

    public Answer fromAnswerInsertDTO(AnswerInsertDTO dto) {
        checkIfNull(dto);

        Answer entity = new Answer();
        if (dto.getExamRatedId() != null) {
            entity.setExamRated(new ExamRated(dto.getExamRatedId()));
        }
        if (dto.getExamQuestionId() != null) {
            entity.setExamQuestion(new ExamQuestion(dto.getExamQuestionId()));
        }
        if (dto.getPossibleAnswerId() != null) {
            entity.setPossibleAnswer(new PossibleAnswer(dto.getPossibleAnswerId()));
        }
        entity.setActive(Boolean.TRUE);

        return entity;
    }

}
