package com.example.demo.mappers;

import com.example.demo.dtos.QuestionTypeViewItemDTO;
import com.example.demo.entities.QuestionType;
import org.springframework.stereotype.Component;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class QuestionTypeMapper {

    public QuestionTypeViewItemDTO toQuestionTypeViewItemDTO(QuestionType entity) {
        checkIfNull(entity);

        QuestionTypeViewItemDTO dto = new QuestionTypeViewItemDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setActive(entity.getActive());

        return dto;
    }

}
