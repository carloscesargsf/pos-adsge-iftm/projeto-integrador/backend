package com.example.demo.mappers;

import com.example.demo.dtos.CategoryInsertDTO;
import com.example.demo.dtos.CategoryViewItemDTO;
import com.example.demo.entities.Category;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class CategoryMapper {

    public CategoryViewItemDTO toCategoryViewItemDTO(Category entity) {
        checkIfNull(entity);

        CategoryViewItemDTO dto = new CategoryViewItemDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        if (entity.getParentCategory() != null) {
            dto.setParentCategoryId(entity.getParentCategory().getId());
        }
        dto.setActive(entity.getActive());
        dto.setChildCategories(entity.getChildCategories()
                .stream()
                .map(e -> toCategoryViewItemDTO(e))
                .collect(Collectors.toList()));

        return dto;
    }

    public Category fromCategoryInsertDTO(CategoryInsertDTO dto) {
        checkIfNull(dto);

        Category entity = new Category();

        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        if (dto.getParentCategoryId() != null) {
            entity.setParentCategory(new Category(dto.getParentCategoryId()));
        }
        entity.setActive(Boolean.TRUE);

        return entity;
    }

}
