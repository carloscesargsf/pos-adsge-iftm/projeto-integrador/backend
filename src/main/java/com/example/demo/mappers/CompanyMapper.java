package com.example.demo.mappers;

import com.example.demo.dtos.CompanyInsertDTO;
import com.example.demo.dtos.CompanyViewItemDTO;
import com.example.demo.entities.Company;
import com.example.demo.entities.Person;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Gender;
import com.example.demo.enums.Role;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class CompanyMapper {

    private final PersonMapper personMapper;

    public CompanyMapper(PersonMapper personMapper) {
        this.personMapper = personMapper;
    }

    public CompanyViewItemDTO toCompanyViewItemDTO(Company entity) {
        checkIfNull(entity);

        CompanyViewItemDTO dto = new CompanyViewItemDTO();

        dto.setId(entity.getId());
        dto.setCompanyName(entity.getCompanyName());
        dto.setTradingName(entity.getTradingName());
        dto.setCnpj(entity.getCnpj());
        dto.setActive(entity.getActive());

        return dto;
    }

    public Company fromCompanyInsertDTO(CompanyInsertDTO dto) {
        checkIfNull(dto);

        Company entity = new Company();

        entity.setCompanyName(dto.getCompanyName());
        entity.setTradingName(dto.getTradingName());
        entity.setCnpj(dto.getCnpj());
        entity.setActive(Boolean.TRUE);

        Person person = new Person();

        person.setName(dto.getAdmin().getName());
        person.setCpf(dto.getAdmin().getCpf());
        person.setGender(Gender.OPTIONS.get(dto.getAdmin().getGender()));
        person.setEmail(dto.getAdmin().getEmail());
        person.setPassword(dto.getAdmin().getPassword());
        person.setActive(Boolean.TRUE);
        person.setPersonRoles(Arrays.asList(Role.ADMIN.getCode())
                .stream()
                .map(role -> {
                    PersonRole personRole = new PersonRole();

                    personRole.setPerson(person);
                    personRole.setCompany(entity);
                    personRole.setRole(Role.OPTIONS.get(role));
                    personRole.setActive(Boolean.TRUE);

                    return personRole;
                })
                .collect(Collectors.toList()));
        entity.setActive(Boolean.TRUE);
        entity.setPeople(person.getPersonRoles());

        return entity;
    }

}
