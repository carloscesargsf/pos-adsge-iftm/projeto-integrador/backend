package com.example.demo.mappers;

import com.example.demo.dtos.PersonInsertDTO;
import com.example.demo.dtos.PersonViewItemDTO;
import com.example.demo.dtos.PersonViewItemWithTokenDTO;
import com.example.demo.entities.Person;
import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Gender;
import com.example.demo.enums.Role;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static com.example.demo.utils.CheckUtils.checkIfNull;
import static com.example.demo.utils.UserUtils.getPrincipal;

@Component
public class PersonMapper {

    private final PersonRoleMapper personRoleMapper;

    public PersonMapper(PersonRoleMapper personRoleMapper) {
        this.personRoleMapper = personRoleMapper;
    }

    public PersonViewItemDTO toPersonViewItemDTO(Person entity) {
        checkIfNull(entity);

        PersonViewItemDTO dto = new PersonViewItemDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCpf(entity.getCpf());
        if (entity.getGender() != null) {
            dto.setGender(entity.getGender().getLabel());
        }
        dto.setEmail(entity.getEmail());
        dto.setActive(entity.getActive());
        dto.setPersonRoles(entity.getPersonRoles()
                .stream()
                .map(personRoleMapper::toPersonRoleViewItemDTO)
                .collect(Collectors.toList()));

        return dto;
    }

    public PersonViewItemWithTokenDTO toPersonViewItemWithTokenDTO(Person entity) {
        checkIfNull(entity);

        PersonViewItemWithTokenDTO dto = new PersonViewItemWithTokenDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCpf(entity.getCpf());
        if (entity.getGender() != null) {
            dto.setGender(entity.getGender().getLabel());
        }
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setActive(entity.getActive());

        return dto;
    }

    public Person fromPersonInsertDTO(PersonInsertDTO dto) {
        checkIfNull(dto);

        if (getPrincipal().getPersonRoles() == null || getPrincipal().getPersonRoles().get(0) == null
                || getPrincipal().getPersonRoles().get(0).getCompany() == null) {
            throw new IllegalArgumentException("Logged user is not associated with a company!");
        }

        Person entity = new Person();

        entity.setName(dto.getName());
        entity.setCpf(dto.getCpf());
        entity.setGender(Gender.OPTIONS.get(dto.getGender()));
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setPersonRoles(dto.getRoles()
                .stream()
                .map(role -> {
                    PersonRole personRole = new PersonRole();

                    personRole.setPerson(entity);
                    personRole.setCompany(getPrincipal().getPersonRoles().get(0).getCompany());
                    personRole.setRole(Role.OPTIONS.get(role));
                    personRole.setActive(Boolean.TRUE);

                    return personRole;
                })
                .collect(Collectors.toList()));
        entity.setActive(Boolean.TRUE);

        return entity;
    }

}
