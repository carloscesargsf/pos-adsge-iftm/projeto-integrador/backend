package com.example.demo.mappers;

import com.example.demo.dtos.ExamInsertDTO;
import com.example.demo.dtos.ExamViewItemDTO;
import com.example.demo.entities.Exam;
import com.example.demo.entities.ExamQuestion;
import com.example.demo.entities.ExamRated;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class ExamMapper {

    private final ExamQuestionMapper examQuestionMapper;

    private final ExamRatedMapper examRatedMapper;

    public ExamMapper(ExamQuestionMapper examQuestionMapper, ExamRatedMapper examRatedMapper) {
        this.examQuestionMapper = examQuestionMapper;
        this.examRatedMapper = examRatedMapper;
    }

    public ExamViewItemDTO toExamViewItemDTO(Exam entity) {
        checkIfNull(entity);

        ExamViewItemDTO dto = new ExamViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getEvaluator() != null && entity.getEvaluator().getPerson() != null) {
            dto.setEvaluatorId(entity.getEvaluator().getPerson().getId());
            dto.setEvaluatorName(entity.getEvaluator().getPerson().getName());
            if (entity.getEvaluator().getPerson().getGender() != null) {
                dto.setEvaluatorGender(entity.getEvaluator().getPerson().getGender().getLabel());
            }
        }
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setBeginDate(entity.getBeginDate());
        dto.setEndDate(entity.getEndDate());
        dto.setGrade(entity.getGrade());
        dto.setActive(entity.getActive());
        dto.setQuestions(entity.getExamQuestions()
                .stream()
                .map(examQuestion -> examQuestionMapper.toExamQuestionViewItemDTO(examQuestion))
                .collect(Collectors.toSet()));
        dto.setRated(entity.getExamRated()
                .stream()
                .map(examRated -> examRatedMapper.toExamRatedViewItemDTO(examRated))
                .collect(Collectors.toSet()));

        return dto;
    }

    public Exam fromExamInsertDTO(ExamInsertDTO dto) {
        checkIfNull(dto);

        Exam entity = new Exam();

        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setBeginDate(dto.getBeginDate());
        entity.setEndDate(dto.getEndDate());
        entity.setGrade(dto.getGrade());
        entity.setActive(Boolean.TRUE);
        entity.setExamQuestions(dto.getQuestionIds()
                .stream()
                .map(questionId -> new ExamQuestion(entity, questionId))
                .collect(Collectors.toList()));
        entity.setExamRated(dto.getRatedIds()
                .stream()
                .map(ratedId -> new ExamRated(entity, ratedId))
                .collect(Collectors.toList()));

        return entity;
    }

}
