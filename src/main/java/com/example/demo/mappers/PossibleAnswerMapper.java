package com.example.demo.mappers;

import com.example.demo.dtos.PossibleAnswerInsertDTO;
import com.example.demo.dtos.PossibleAnswerViewItemDTO;
import com.example.demo.entities.PossibleAnswer;
import com.example.demo.entities.Question;
import org.springframework.stereotype.Component;

import static com.example.demo.utils.CheckUtils.checkIfNull;

@Component
public class PossibleAnswerMapper {

    public PossibleAnswerViewItemDTO toPossibleAnswerViewItemDTO(PossibleAnswer entity) {
        checkIfNull(entity);

        PossibleAnswerViewItemDTO dto = new PossibleAnswerViewItemDTO();

        dto.setId(entity.getId());
        if (entity.getQuestion() != null) {
            dto.setQuestionId(entity.getQuestion().getId());
            dto.setQuestionTitle(entity.getQuestion().getTitle());
        }
        dto.setDescription(entity.getDescription());
        dto.setCorrect(entity.getCorrect());
        dto.setActive(entity.getActive());

        return dto;
    }

    public PossibleAnswer fromPossibleAnswerInsertDTO(PossibleAnswerInsertDTO dto) {
        checkIfNull(dto);

        PossibleAnswer entity = new PossibleAnswer();

        if (dto.getQuestionId() != null) {
            entity.setQuestion(new Question(dto.getQuestionId()));
        }
        entity.setDescription(dto.getDescription());
        entity.setCorrect(dto.getCorrect());
        entity.setActive(Boolean.TRUE);

        return entity;
    }

}
