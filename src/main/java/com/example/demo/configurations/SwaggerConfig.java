package com.example.demo.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Optional;

@Configuration
@EnableSwagger2
@Profile("!test")
public class SwaggerConfig {

    @Bean
    public Docket newsApiControllers() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Demo - Controllers")
                .securitySchemes(Arrays.asList(apiKey()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controllers"))
                .paths(PathSelectors.any())
                .build()
                .genericModelSubstitutes(Optional.class)
                .apiInfo(apiInfo());
    }

    @Bean
    public Docket securityApiControllers() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Demo - Security")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.security.controllers"))
                .paths(PathSelectors.any())
                .build()
                .genericModelSubstitutes(Optional.class)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Demo")
                .description("Documentação API Demo")
                .version("1.0")
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey(HttpHeaders.AUTHORIZATION, HttpHeaders.AUTHORIZATION, "header");
    }

}

