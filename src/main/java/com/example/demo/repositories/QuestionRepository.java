package com.example.demo.repositories;

import com.example.demo.entities.Question;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends BaseRepository<Question, Long> {
}
