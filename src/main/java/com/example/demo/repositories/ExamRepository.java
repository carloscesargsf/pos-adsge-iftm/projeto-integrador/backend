package com.example.demo.repositories;

import com.example.demo.entities.Exam;
import org.springframework.stereotype.Repository;

@Repository
public interface ExamRepository extends BaseRepository<Exam, Long> {
}
