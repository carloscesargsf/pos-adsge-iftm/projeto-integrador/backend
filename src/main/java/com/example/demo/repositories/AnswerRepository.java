package com.example.demo.repositories;

import com.example.demo.entities.Answer;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends BaseRepository<Answer, Long> {
}
