package com.example.demo.repositories;

import com.example.demo.entities.ExamRated;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExamRatedRepository extends BaseRepository<ExamRated, Long> {

    Optional<ExamRated> findByExamIdAndRatedId(Long examId, Long ratedId);

}
