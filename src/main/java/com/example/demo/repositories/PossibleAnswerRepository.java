package com.example.demo.repositories;

import com.example.demo.entities.PossibleAnswer;
import org.springframework.stereotype.Repository;

@Repository
public interface PossibleAnswerRepository extends BaseRepository<PossibleAnswer, Long> {
}
