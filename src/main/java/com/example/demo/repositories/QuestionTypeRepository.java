package com.example.demo.repositories;

import com.example.demo.entities.QuestionType;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionTypeRepository extends BaseRepository<QuestionType, Long> {
}
