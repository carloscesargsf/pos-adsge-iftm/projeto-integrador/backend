package com.example.demo.repositories;

import com.example.demo.entities.ExamQuestion;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExamQuestionRepository extends BaseRepository<ExamQuestion, Long> {

    Optional<ExamQuestion> findByExamIdAndQuestionId(Long examId, Long questionId);

}
