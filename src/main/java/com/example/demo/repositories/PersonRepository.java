package com.example.demo.repositories;

import com.example.demo.entities.Person;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends BaseRepository<Person, Long> {

    Optional<Person> findByEmail(String email);

}
