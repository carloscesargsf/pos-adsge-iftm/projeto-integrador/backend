package com.example.demo.repositories;

import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;
import com.example.demo.hql.PostgreSQLEnumType;
import org.hibernate.annotations.TypeDef;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@TypeDef(name = "role", typeClass = PostgreSQLEnumType.class)
public interface PersonRoleRepository extends BaseRepository<PersonRole, Long> {

    Optional<PersonRole> findByPersonIdAndCompanyIdAndRole(Long personId, Long companyId, Role role);

}
