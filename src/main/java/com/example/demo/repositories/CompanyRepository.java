package com.example.demo.repositories;

import com.example.demo.entities.Company;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends BaseRepository<Company, Long> {
}
