package com.example.demo.constants;

public class ExceptionMessages {

    private ExceptionMessages() {
        // $COVERAGE-IGNORE$
    }

    public static final String BAD_CREDENTIALS = "Bad credentials!";

    public static final String INVALID_METHOD_ARGUMENT = "Invalid method argument(s).";

    public static final String PARAM_MUST_NO_BE_NULL = "Param %s must no be null";

    public static final String INFORMED_ID_MUST_BE_EQUAL_FROM_THE_OBJECT = "Informed ID must be equal from the object.";
    public static final String ID_IS_MANDATORY = "ID is mandatory";

    public static final String USER_DOESNT_HAVE_THIS_PERMISSION = "User doesn't have this permission: %s";

    public static final String PERSON_WITH_ID_NOT_FOUND = "Person with ID '%d' not found.";
    public static final String COMPANY_WITH_ID_NOT_FOUND = "Company with ID '%d' not found.";
    public static final String PERSON_ROLE_WITH_ID_NOT_FOUND = "Person Role with ID '%d' not found.";
    public static final String CATEGORY_WITH_ID_NOT_FOUND = "Category with ID '%d' not found.";
    public static final String QUESTION_TYPE_WITH_ID_NOT_FOUND = "Question Type with ID '%d' not found.";
    public static final String QUESTION_WITH_ID_NOT_FOUND = "Question with ID '%d' not found.";
    public static final String POSSIBLE_ANSWER_WITH_ID_NOT_FOUND = "Possible Answer with ID '%d' not found.";
    public static final String EXAM_WITH_ID_NOT_FOUND = "Exam with ID '%d' not found.";
    public static final String EXAM_RATED_WITH_ID_NOT_FOUND = "Exam Rated with ID '%d' not found.";
    public static final String EXAM_QUESTION_WITH_ID_NOT_FOUND = "Exam Question with ID '%d' not found.";
    public static final String ANSWER_WITH_ID_NOT_FOUND = "Answer with ID '%d' not found.";

}
