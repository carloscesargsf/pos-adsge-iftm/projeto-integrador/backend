package com.example.demo.controllers;

import com.example.demo.dtos.ExamInsertDTO;
import com.example.demo.dtos.ExamUpdateDTO;
import com.example.demo.dtos.ExamViewItemDTO;
import com.example.demo.filters.ExamFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.services.ExamService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/exams")
public class ExamController {

    private final ExamService examService;

    public ExamController(ExamService examService) {
        this.examService = examService;
    }

    @GetMapping
    public ResponseEntity findAll(ExamFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(examService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(examService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody ExamInsertDTO dto) {
        ExamViewItemDTO viewItemDTO = examService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody ExamUpdateDTO dto) {
        examService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        examService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        examService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
