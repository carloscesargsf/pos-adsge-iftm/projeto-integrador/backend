package com.example.demo.controllers;

import com.example.demo.dtos.CategoryInsertDTO;
import com.example.demo.dtos.CategoryUpdateDTO;
import com.example.demo.dtos.CategoryViewItemDTO;
import com.example.demo.filters.CategoryFilter;
import com.example.demo.services.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity findAll(CategoryFilter filters) {
        return ResponseEntity.ok(categoryService.findAll(filters));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(categoryService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody CategoryInsertDTO dto) {
        CategoryViewItemDTO viewItemDTO = categoryService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody CategoryUpdateDTO dto) {
        categoryService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        categoryService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        categoryService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
