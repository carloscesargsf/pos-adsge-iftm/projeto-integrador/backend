package com.example.demo.controllers;

import com.example.demo.dtos.ExamQuestionInsertDTO;
import com.example.demo.dtos.ExamQuestionUpdateDTO;
import com.example.demo.dtos.ExamQuestionViewItemDTO;
import com.example.demo.filters.ExamQuestionFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.services.ExamQuestionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/exams-questions")
public class ExamQuestionController {

    private final ExamQuestionService examQuestionService;

    public ExamQuestionController(ExamQuestionService examQuestionService) {
        this.examQuestionService = examQuestionService;
    }

    @GetMapping
    public ResponseEntity findAll(ExamQuestionFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(examQuestionService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(examQuestionService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody ExamQuestionInsertDTO dto) {
        ExamQuestionViewItemDTO viewItemDTO = examQuestionService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody ExamQuestionUpdateDTO dto) {
        examQuestionService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        examQuestionService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        examQuestionService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
