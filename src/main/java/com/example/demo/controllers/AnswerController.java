package com.example.demo.controllers;

import com.example.demo.dtos.AnswerInsertDTO;
import com.example.demo.dtos.AnswerUpdateDTO;
import com.example.demo.dtos.AnswerViewItemDTO;
import com.example.demo.filters.AnswerFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.services.AnswerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/answers")
public class AnswerController {

    private final AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping
    public ResponseEntity findAll(AnswerFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(answerService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(answerService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody AnswerInsertDTO dto) {
        AnswerViewItemDTO viewItemDTO = answerService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody AnswerUpdateDTO dto) {
        answerService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        answerService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        answerService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
