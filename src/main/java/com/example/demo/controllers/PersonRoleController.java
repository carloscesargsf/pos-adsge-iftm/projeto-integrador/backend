package com.example.demo.controllers;

import com.example.demo.dtos.PersonRoleInsertDTO;
import com.example.demo.dtos.PersonRoleUpdateDTO;
import com.example.demo.dtos.PersonRoleViewItemDTO;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PersonRoleFilter;
import com.example.demo.services.PersonRoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/people-roles")
public class PersonRoleController {

    private final PersonRoleService personRoleService;

    public PersonRoleController(PersonRoleService personRoleService) {
        this.personRoleService = personRoleService;
    }

    @GetMapping
    public ResponseEntity findAll(PersonRoleFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(personRoleService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(personRoleService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody PersonRoleInsertDTO dto) {
        PersonRoleViewItemDTO viewItemDTO = personRoleService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody PersonRoleUpdateDTO dto) {
        personRoleService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        personRoleService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        personRoleService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
