package com.example.demo.controllers;

import com.example.demo.filters.PageInfo;
import com.example.demo.filters.QuestionTypeFilter;
import com.example.demo.services.QuestionTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/question-types")
public class QuestionTypeController {

    private final QuestionTypeService questionTypeService;

    public QuestionTypeController(QuestionTypeService questionTypeService) {
        this.questionTypeService = questionTypeService;
    }

    @GetMapping
    public ResponseEntity findAll(QuestionTypeFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(questionTypeService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(questionTypeService.findById(id));
    }

}
