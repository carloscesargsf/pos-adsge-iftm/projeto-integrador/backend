package com.example.demo.controllers;

import com.example.demo.dtos.PossibleAnswerInsertDTO;
import com.example.demo.dtos.PossibleAnswerUpdateDTO;
import com.example.demo.dtos.PossibleAnswerViewItemDTO;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PossibleAnswerFilter;
import com.example.demo.services.PossibleAnswerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/possible-answers")
public class PossibleAnswerController {

    private final PossibleAnswerService possibleAnswerService;

    public PossibleAnswerController(PossibleAnswerService possibleAnswerService) {
        this.possibleAnswerService = possibleAnswerService;
    }

    @GetMapping
    public ResponseEntity findAll(PossibleAnswerFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(possibleAnswerService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(possibleAnswerService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody PossibleAnswerInsertDTO dto) {
        PossibleAnswerViewItemDTO viewItemDTO = possibleAnswerService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody PossibleAnswerUpdateDTO dto) {
        possibleAnswerService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        possibleAnswerService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        possibleAnswerService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
