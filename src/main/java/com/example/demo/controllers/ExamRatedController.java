package com.example.demo.controllers;

import com.example.demo.dtos.ExamRatedInsertDTO;
import com.example.demo.dtos.ExamRatedUpdateDTO;
import com.example.demo.dtos.ExamRatedViewItemDTO;
import com.example.demo.filters.ExamRatedFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.services.ExamRatedService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/exams-rated")
public class ExamRatedController {

    private final ExamRatedService examRatedService;

    public ExamRatedController(ExamRatedService examRatedService) {
        this.examRatedService = examRatedService;
    }

    @GetMapping
    public ResponseEntity findAll(ExamRatedFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(examRatedService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(examRatedService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody ExamRatedInsertDTO dto) {
        ExamRatedViewItemDTO viewItemDTO = examRatedService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody ExamRatedUpdateDTO dto) {
        examRatedService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        examRatedService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        examRatedService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
