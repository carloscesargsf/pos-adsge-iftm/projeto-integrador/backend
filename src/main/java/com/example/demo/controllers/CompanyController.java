package com.example.demo.controllers;

import com.example.demo.dtos.CompanyInsertDTO;
import com.example.demo.dtos.CompanyUpdateDTO;
import com.example.demo.dtos.CompanyViewItemDTO;
import com.example.demo.filters.CompanyFilter;
import com.example.demo.filters.PageInfo;
import com.example.demo.services.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public ResponseEntity findAll(CompanyFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(companyService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(companyService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody CompanyInsertDTO dto) {
        CompanyViewItemDTO viewItemDTO = companyService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody CompanyUpdateDTO dto) {
        companyService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        companyService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        companyService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
