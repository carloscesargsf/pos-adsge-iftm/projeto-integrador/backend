package com.example.demo.controllers;

import com.example.demo.dtos.QuestionInsertDTO;
import com.example.demo.dtos.QuestionUpdateDTO;
import com.example.demo.dtos.QuestionViewItemDTO;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.QuestionFilter;
import com.example.demo.services.QuestionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping
    public ResponseEntity findAll(QuestionFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(questionService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(questionService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody QuestionInsertDTO dto) {
        QuestionViewItemDTO viewItemDTO = questionService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody QuestionUpdateDTO dto) {
        questionService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        questionService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        questionService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
