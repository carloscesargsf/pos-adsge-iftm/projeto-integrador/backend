package com.example.demo.controllers;

import com.example.demo.dtos.PersonInsertDTO;
import com.example.demo.dtos.PersonUpdateDTO;
import com.example.demo.dtos.PersonViewItemDTO;
import com.example.demo.filters.PageInfo;
import com.example.demo.filters.PersonFilter;
import com.example.demo.services.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/people")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public ResponseEntity findAll(PersonFilter filters,
                                  PageInfo pageInfo) {
        return ResponseEntity.ok(personService.findAll(filters, pageInfo));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(personService.findById(id));
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody PersonInsertDTO dto) {
        PersonViewItemDTO viewItemDTO = personService.create(dto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(viewItemDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(viewItemDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody PersonUpdateDTO dto) {
        personService.update(id, dto);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/activate")
    public ResponseEntity activate(@PathVariable(name = "id") Long id) {
        personService.activate(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/inactivate")
    public ResponseEntity inactivate(@PathVariable(name = "id") Long id) {
        personService.inactivate(id);
        return ResponseEntity.noContent().build();
    }

}
