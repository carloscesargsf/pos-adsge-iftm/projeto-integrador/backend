package com.example.demo.utils;

import com.example.demo.entities.Person;
import com.example.demo.entities.PersonRole;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;

public final class UserUtils {

    private UserUtils() {
        // $COVERAGE-IGNORE$
    }

    public static Person getPrincipal() {
        return (Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }


    public static boolean hasAuthority(Long companyId, String role) {
        return companyId != null && role != null
                && getPrincipal().getAuthorities().stream().anyMatch(
                personRole -> ((PersonRole) personRole).getRole().getCode().equals(role)
                        && ((PersonRole) personRole).getCompany().getId().equals(companyId));
    }

    public static boolean hasAuthority(String role) {
        return role != null
                && getPrincipal().getAuthorities().stream().anyMatch(
                personRole -> ((PersonRole) personRole).getRole().getCode().equals(role));
    }

    public static boolean hasAnyAuthorities(String... roles) {
        return roles != null && Arrays.stream(roles).anyMatch(role -> getPrincipal().getAuthorities()
                .stream().anyMatch(personRole -> ((PersonRole) personRole).getRole().getCode().equals(role)));
    }


}
