package com.example.demo.utils;

import com.example.demo.exceptions.IllegalArgumentException;

import static com.example.demo.constants.ExceptionMessages.PARAM_MUST_NO_BE_NULL;

public class CheckUtils {

    public static <E> void checkIfNull(E object) {
        if (object == null) {
            throw new IllegalArgumentException(PARAM_MUST_NO_BE_NULL, object.getClass().getName());
        }
    }

}
