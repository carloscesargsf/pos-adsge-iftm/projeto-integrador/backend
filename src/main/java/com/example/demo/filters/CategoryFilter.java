package com.example.demo.filters;

import com.example.demo.entities.Category;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class CategoryFilter extends BaseFilter<Category> {

    private Optional<String> name = empty();

    private Optional<Boolean> active = empty();

    public CategoryFilter filterByName(String name) {
        setName(ofNullable(name));
        return this;
    }

    public CategoryFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<Category> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        name.ifPresent(cn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("companyName")), "%" + cn.toLowerCase() + "%")));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
        predicates.add(criteriaBuilder.isNull(root.get("parentCategory")));
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
