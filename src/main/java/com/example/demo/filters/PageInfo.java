package com.example.demo.filters;

import java.util.Optional;

import static java.util.Optional.empty;

public class PageInfo {

    private Optional<Integer> page = empty();

    private Optional<Integer> pageSize = empty();

    private Optional<String[]> sortBy = empty();

    private Boolean ascending = true;

    public PageInfo() {
    }

    public PageInfo(Optional<Integer> page, Optional<Integer> pageSize, Optional<String[]> sortBy, Boolean ascending) {
        setPage(page);
        setPageSize(pageSize);
        setSortBy(sortBy);
        setAscending(ascending);
    }

    public Optional<Integer> getPage() {
        return page;
    }

    public void setPage(Optional<Integer> page) {
        this.page = page;
    }

    public Optional<Integer> getPageSize() {
        return pageSize;
    }

    public void setPageSize(Optional<Integer> pageSize) {
        this.pageSize = pageSize;
    }

    public Optional<String[]> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String[]> sortBy) {
        this.sortBy = sortBy;
    }

    public Boolean getAscending() {
        return ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

}
