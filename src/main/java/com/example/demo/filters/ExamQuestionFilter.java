package com.example.demo.filters;

import com.example.demo.entities.ExamQuestion;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class ExamQuestionFilter extends BaseFilter<ExamQuestion> {

    private Optional<Long> examId = empty();

    private Optional<Long> questionId = empty();

    private Optional<Boolean> active = empty();

    public ExamQuestionFilter filterByExamId(Long examId) {
        setExamId(ofNullable(examId));
        return this;
    }

    public ExamQuestionFilter filterByQuestionId(Long questionId) {
        setQuestionId(ofNullable(questionId));
        return this;
    }

    public ExamQuestionFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<ExamQuestion> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        examId.ifPresent(ei -> predicates.add(criteriaBuilder.equal(root.get("exam").get("id"), ei)));
        questionId.ifPresent(ri -> predicates.add(criteriaBuilder.equal(root.get("question").get("id"), ri)));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<Long> getExamId() {
        return examId;
    }

    public void setExamId(Optional<Long> examId) {
        this.examId = examId;
    }

    public Optional<Long> getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Optional<Long> questionId) {
        this.questionId = questionId;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
