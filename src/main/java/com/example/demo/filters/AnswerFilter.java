package com.example.demo.filters;

import com.example.demo.entities.Answer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class AnswerFilter extends BaseFilter<Answer> {

    private Optional<Long> examId = empty();

    private Optional<List<Long>> ratedId = empty();

    private Optional<List<Long>> questionsId = empty();

    private Optional<List<Long>> possibleAnswersId = empty();

    private Optional<Boolean> active = empty();

    public AnswerFilter filterByExamId(Long examId) {
        setExamId(ofNullable(examId));
        return this;
    }

    public AnswerFilter filterByRatedId(List<Long> ratedId) {
        setRatedId(ofNullable(ratedId));
        return this;
    }

    public AnswerFilter filterByQuestionId(List<Long> questionId) {
        setQuestionsId(ofNullable(questionId));
        return this;
    }

    public AnswerFilter filterByPossibleAnswerId(List<Long> possibleAnswerId) {
        setPossibleAnswersId(ofNullable(possibleAnswerId));
        return this;
    }

    public AnswerFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<Answer> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        examId.ifPresent(ei -> predicates.add(criteriaBuilder.equal(root.get("examRated").get("exam").get("id"), ei)));
        ratedId.ifPresent(ri -> predicates.add(root.get("examRated").get("rated").get("id").in(ri)));
        questionsId.ifPresent(qi -> predicates.add(root.get("examQuestion").get("question").get("id").in(qi)));
        possibleAnswersId.ifPresent(pai -> predicates.add(root.get("possibleAnswer").get("id").in(pai)));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<Long> getExamId() {
        return examId;
    }

    public void setExamId(Optional<Long> examId) {
        this.examId = examId;
    }

    public Optional<List<Long>> getRatedId() {
        return ratedId;
    }

    public void setRatedId(Optional<List<Long>> ratedId) {
        this.ratedId = ratedId;
    }

    public Optional<List<Long>> getQuestionsId() {
        return questionsId;
    }

    public void setQuestionsId(Optional<List<Long>> questionsId) {
        this.questionsId = questionsId;
    }

    public Optional<List<Long>> getPossibleAnswersId() {
        return possibleAnswersId;
    }

    public void setPossibleAnswersId(Optional<List<Long>> possibleAnswersId) {
        this.possibleAnswersId = possibleAnswersId;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
