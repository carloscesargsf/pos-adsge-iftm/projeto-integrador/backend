package com.example.demo.filters;

import com.example.demo.entities.PersonRole;
import com.example.demo.enums.Role;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class PersonRoleFilter extends BaseFilter<PersonRole> {

    private Optional<String> personName = empty();

    private Optional<String> personEmail = empty();

    private Optional<Long> companyId = empty();

    private Optional<String> companyName = empty();

    private Optional<String> companyTradingName = empty();

    private Optional<Role> role = empty();

    private Optional<Boolean> active = empty();

    public PersonRoleFilter filterByPersonName(String personName) {
        setPersonName(ofNullable(personName));
        return this;
    }

    public PersonRoleFilter filterByPersonEmail(String personEmail) {
        setPersonEmail(ofNullable(personEmail));
        return this;
    }

    public PersonRoleFilter filterByCompanyId(Long companyId) {
        setCompanyId(ofNullable(companyId));
        return this;
    }

    public PersonRoleFilter filterByCompanyName(String companyName) {
        setCompanyName(ofNullable(companyName));
        return this;
    }

    public PersonRoleFilter filterByCompanyTradingName(String companyTradingName) {
        setCompanyTradingName(ofNullable(companyTradingName));
        return this;
    }

    public PersonRoleFilter filterByRole(String role) {
        setRole(ofNullable(role));
        return this;
    }

    public PersonRoleFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<PersonRole> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        personName.ifPresent(pn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("person").get("name")), "%" + pn.toLowerCase() + "%")));
        personEmail.ifPresent(pe -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("person").get("email")), "%" + pe.toLowerCase() + "%")));
        companyId.ifPresent(ci -> predicates.add(
                criteriaBuilder.equal(root.get("company").get("id"), ci)));
        companyName.ifPresent(cn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("company").get("companyName")), "%" + cn.toLowerCase() + "%")));
        companyTradingName.ifPresent(ctn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("company").get("tradingName")), "%" + ctn.toLowerCase() + "%")));
        role.ifPresent(r -> predicates.add(criteriaBuilder.equal(root.get("role"), r)));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<String> getPersonName() {
        return personName;
    }

    public void setPersonName(Optional<String> personName) {
        this.personName = personName;
    }

    public Optional<String> getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(Optional<String> personEmail) {
        this.personEmail = personEmail;
    }

    public Optional<Long> getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Optional<Long> companyId) {
        this.companyId = companyId;
    }

    public Optional<String> getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Optional<String> companyName) {
        this.companyName = companyName;
    }

    public Optional<String> getCompanyTradingName() {
        return companyTradingName;
    }

    public void setCompanyTradingName(Optional<String> companyTradingName) {
        this.companyTradingName = companyTradingName;
    }

    public Optional<Role> getRole() {
        return role;
    }

    public void setRole(Optional<String> role) {
        if (role.isPresent()) {
            this.role = ofNullable(Role.OPTIONS.get(role.get()));
        }
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
