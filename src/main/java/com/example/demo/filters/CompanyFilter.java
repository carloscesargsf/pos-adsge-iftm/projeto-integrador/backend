package com.example.demo.filters;

import com.example.demo.entities.Company;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class CompanyFilter extends BaseFilter<Company> {

    private Optional<String> companyName = empty();

    private Optional<String> tradingName = empty();

    private Optional<Boolean> active = empty();

    public CompanyFilter filterByCompanyName(String companyName) {
        setCompanyName(ofNullable(companyName));
        return this;
    }

    public CompanyFilter filterByTradingName(String tradingName) {
        setTradingName(ofNullable(tradingName));
        return this;
    }

    public CompanyFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<Company> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        companyName.ifPresent(cn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("companyName")), "%" + cn.toLowerCase() + "%")));
        tradingName.ifPresent(tn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("tradingName")), "%" + tn.toLowerCase() + "%")));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<String> getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Optional<String> companyName) {
        this.companyName = companyName;
    }

    public Optional<String> getTradingName() {
        return tradingName;
    }

    public void setTradingName(Optional<String> tradingName) {
        this.tradingName = tradingName;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
