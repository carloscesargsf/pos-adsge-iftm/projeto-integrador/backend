package com.example.demo.filters;

import com.example.demo.entities.ExamRated;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class ExamRatedFilter extends BaseFilter<ExamRated> {

    private Optional<Long> examId = empty();

    private Optional<Long> ratedId = empty();

    private Optional<Boolean> active = empty();

    public ExamRatedFilter filterByExamId(Long examId) {
        setExamId(ofNullable(examId));
        return this;
    }

    public ExamRatedFilter filterByRatedId(Long ratedId) {
        setRatedId(ofNullable(ratedId));
        return this;
    }

    public ExamRatedFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<ExamRated> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        examId.ifPresent(ei -> predicates.add(criteriaBuilder.equal(root.get("exam").get("id"), ei)));
        ratedId.ifPresent(ri -> predicates.add(criteriaBuilder.equal(root.get("rated").get("id"), ri)));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<Long> getExamId() {
        return examId;
    }

    public void setExamId(Optional<Long> examId) {
        this.examId = examId;
    }

    public Optional<Long> getRatedId() {
        return ratedId;
    }

    public void setRatedId(Optional<Long> ratedId) {
        this.ratedId = ratedId;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
