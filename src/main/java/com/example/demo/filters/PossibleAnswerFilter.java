package com.example.demo.filters;

import com.example.demo.entities.PossibleAnswer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class PossibleAnswerFilter extends BaseFilter<PossibleAnswer> {

    private Optional<Long> questionId = empty();

    private Optional<Boolean> correct = empty();

    private Optional<Boolean> active = empty();

    public PossibleAnswerFilter filterByQuestionId(Long questionId) {
        setQuestionId(ofNullable(questionId));
        return this;
    }

    public PossibleAnswerFilter filterByCorrect(Boolean correct) {
        setCorrect(ofNullable(correct));
        return this;
    }

    public PossibleAnswerFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<PossibleAnswer> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        questionId.ifPresent(qi -> predicates.add(
                criteriaBuilder.equal(root.get("question").get("id"), qi)));
        correct.ifPresent(c -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), c),
                        criteriaBuilder.equal(root.get("correct"), c))));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<Long> getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Optional<Long> questionId) {
        this.questionId = questionId;
    }

    public Optional<Boolean> getCorrect() {
        return correct;
    }

    public void setCorrect(Optional<Boolean> correct) {
        this.correct = correct;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
