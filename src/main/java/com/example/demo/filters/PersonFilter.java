package com.example.demo.filters;

import com.example.demo.entities.Person;
import com.example.demo.enums.Role;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class PersonFilter extends BaseFilter<Person> {

    private Optional<String> name = empty();

    private Optional<String> email = empty();

    private Optional<Boolean> active = empty();

    private Optional<Long> companyId = empty();

    private Optional<Role> role = empty();

    public PersonFilter filterByName(String name) {
        setName(ofNullable(name));
        return this;
    }

    public PersonFilter filterByEmail(String email) {
        setEmail(ofNullable(email));
        return this;
    }

    public PersonFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    public PersonFilter filterByCompanyId(Long companyId) {
        setCompanyId(ofNullable(companyId));
        return this;
    }

    public PersonFilter filterByRole(String role) {
        setRole(ofNullable(role));
        return this;
    }

    @Override
    public void defineFilters(Root<Person> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        name.ifPresent(n -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + n.toLowerCase() + "%")));
        email.ifPresent(e -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("email")), "%" + e.toLowerCase() + "%")));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
        companyId.ifPresent(c -> predicates.add(
                criteriaBuilder.equal(root.join("personRoles").get("company").get("id"), c)));
        role.ifPresent(r -> {
            predicates.add(
                    criteriaBuilder.equal(root.join("personRoles").get("role"), r));
        });

        criteriaQuery.distinct(Boolean.TRUE);
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

    public Optional<Long> getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Optional<Long> companyId) {
        this.companyId = companyId;
    }

    public Optional<Role> getRole() {
        return role;
    }

    public void setRole(Optional<String> role) {
        if (role.isPresent()) {
            this.role = ofNullable(Role.OPTIONS.get(role.get()));
        }
    }

}
