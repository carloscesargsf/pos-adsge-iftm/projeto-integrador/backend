package com.example.demo.filters;

import com.example.demo.entities.Question;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class QuestionFilter extends BaseFilter<Question> {

    private Optional<Long> questionTypeId = empty();

    private Optional<Long> categoryId = empty();

    private Optional<Boolean> active = empty();

    public QuestionFilter filterByQuestionTypeId(Long questionTypeId) {
        setQuestionTypeId(ofNullable(questionTypeId));
        return this;
    }

    public QuestionFilter filterByCategoryId(Long categoryId) {
        setCategoryId(ofNullable(categoryId));
        return this;
    }

    public QuestionFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    @Override
    public void defineFilters(Root<Question> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        questionTypeId.ifPresent(qti -> predicates.add(
                criteriaBuilder.equal(root.get("questionType").get("id"), qti)));
        categoryId.ifPresent(ci -> predicates.add(
                criteriaBuilder.equal(root.get("category").get("id"), ci)));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
    }

    public Optional<Long> getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(Optional<Long> questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public Optional<Long> getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Optional<Long> categoryId) {
        this.categoryId = categoryId;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

}
