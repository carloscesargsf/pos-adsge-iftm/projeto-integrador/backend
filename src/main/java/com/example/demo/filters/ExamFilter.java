package com.example.demo.filters;

import com.example.demo.entities.Exam;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class ExamFilter extends BaseFilter<Exam> {

    private Optional<Long> evaluatorId = empty();

    private Optional<Long> ratedId = empty();

    private Optional<String> name = empty();

    private Optional<Boolean> active = empty();

    private Optional<LocalDate> beginDateBegin = empty();

    private Optional<LocalDate> beginDateEnd = empty();

    private Optional<LocalDate> endDateBegin = empty();

    private Optional<LocalDate> endDateEnd = empty();

    private Optional<Long> companyId = empty();

    public ExamFilter filterByEvaluatorId(Long evaluatorId) {
        setEvaluatorId(ofNullable(evaluatorId));
        return this;
    }

    public ExamFilter filterByRatedId(Long ratedId) {
        setRatedId(ofNullable(ratedId));
        return this;
    }

    public ExamFilter filterByName(String name) {
        setName(ofNullable(name));
        return this;
    }

    public ExamFilter filterByActive(Boolean active) {
        setActive(ofNullable(active));
        return this;
    }

    public ExamFilter filterByBeginDate(LocalDate beginDateBegin, LocalDate beginDateEnd) {
        setBeginDateBegin(ofNullable(beginDateBegin));
        setBeginDateEnd(ofNullable(beginDateEnd));
        return this;
    }

    public ExamFilter filterByEndDate(LocalDate endDateBegin, LocalDate endDateEnd) {
        setEndDateBegin(ofNullable(endDateBegin));
        setEndDateEnd(ofNullable(endDateEnd));
        return this;
    }

    public ExamFilter filterByCompanyId(Long companyId) {
        setCompanyId(ofNullable(companyId));
        return this;
    }

    @Override
    public void defineFilters(Root<Exam> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                              List<Predicate> predicates) {
        evaluatorId.ifPresent(ei -> predicates.add(criteriaBuilder.equal(root.get("evaluator").get("id"), ei)));
        ratedId.ifPresent(ei -> predicates.add(criteriaBuilder.equal(root.join("examRated").get("rated").get("id"), ei)));
        name.ifPresent(cn -> predicates.add(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("evaluator").get("person").get("name")),
                        "%" + cn.toLowerCase() + "%")));
        active.ifPresent(a -> predicates.add(
                criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder.literal(Boolean.FALSE), a),
                        criteriaBuilder.equal(root.get("active"), a))));
        beginDateBegin.ifPresent(bdb -> predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("beginDate"), bdb)));
        beginDateEnd.ifPresent(bde -> predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("beginDate"), bde)));
        endDateBegin.ifPresent(edb -> predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("endDate"), edb)));
        endDateEnd.ifPresent(ede -> predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("endDate"), ede)));
        companyId.ifPresent(ci -> predicates.add(criteriaBuilder.equal(root.get("evaluator").get("company").get("id"), ci)));
    }

    public Optional<Long> getEvaluatorId() {
        return evaluatorId;
    }

    public void setEvaluatorId(Optional<Long> evaluatorId) {
        this.evaluatorId = evaluatorId;
    }

    public Optional<Long> getRatedId() {
        return ratedId;
    }

    public void setRatedId(Optional<Long> ratedId) {
        this.ratedId = ratedId;
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<Boolean> getActive() {
        return active;
    }

    public void setActive(Optional<Boolean> active) {
        this.active = active;
    }

    public Optional<LocalDate> getBeginDateBegin() {
        return beginDateBegin;
    }

    public void setBeginDateBegin(Optional<LocalDate> beginDateBegin) {
        this.beginDateBegin = beginDateBegin;
    }

    public Optional<LocalDate> getBeginDateEnd() {
        return beginDateEnd;
    }

    public void setBeginDateEnd(Optional<LocalDate> beginDateEnd) {
        this.beginDateEnd = beginDateEnd;
    }

    public Optional<LocalDate> getEndDateBegin() {
        return endDateBegin;
    }

    public void setEndDateBegin(Optional<LocalDate> endDateBegin) {
        this.endDateBegin = endDateBegin;
    }

    public Optional<LocalDate> getEndDateEnd() {
        return endDateEnd;
    }

    public void setEndDateEnd(Optional<LocalDate> endDateEnd) {
        this.endDateEnd = endDateEnd;
    }

    public Optional<Long> getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Optional<Long> companyId) {
        this.companyId = companyId;
    }

}
