package com.example.demo.filters;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFilter<E> implements Specification<E> {

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        defineFilters(root, criteriaQuery, criteriaBuilder, predicates);

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    public abstract void defineFilters(Root<E> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                       List<Predicate> predicates);

}
