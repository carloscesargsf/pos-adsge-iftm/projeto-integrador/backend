

CREATE OR REPLACE FUNCTION update_updated_at_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TYPE gender AS
    ENUM ('F', 'M');

CREATE TABLE IF NOT EXISTS tb_people (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(200) NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    gender gender NOT NULL,
    email VARCHAR(200) UNIQUE NOT NULL,
    password VARCHAR(200) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER tb_people_update_updated_at_column
BEFORE UPDATE ON tb_people
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_companies (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    company_name VARCHAR(200) NOT NULL,
    trading_name VARCHAR(200) NOT NULL,
    cnpj VARCHAR(14) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER tb_companies_update_updated_at_column
BEFORE UPDATE ON tb_companies
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TYPE role AS
    ENUM ('ADMIN', 'EVALUATOR', 'RATED');

CREATE TABLE tb_people_roles (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    person_id INT8 NOT NULL,
    company_id INT8 NOT NULL,
    role role NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (person_id)
        REFERENCES tb_people (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY (company_id)
        REFERENCES tb_companies (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_people_roles_update_updated_at_column
BEFORE UPDATE ON tb_people_roles
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_categories (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(200) NOT NULL,
    description TEXT NULL,
    parent_category_id INT8 NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (parent_category_id)
        REFERENCES tb_categories (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_categories_update_updated_at_column
BEFORE UPDATE ON tb_categories
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_questions_types (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(200) NOT NULL,
    description TEXT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER tb_questions_types_update_updated_at_column
BEFORE UPDATE ON tb_questions_types
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_questions (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    question_type_id INT8 NOT NULL,
    category_id INT8 NOT NULL,
    title TEXT NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (question_type_id)
        REFERENCES tb_questions_types (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY (category_id)
        REFERENCES tb_categories (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_questions_update_updated_at_column
BEFORE UPDATE ON tb_questions
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_possible_answers (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    question_id INT8 NOT NULL,
    description TEXT NULL,
    correct BOOLEAN NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (question_id)
        REFERENCES tb_questions (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_possible_answers_update_updated_at_column
BEFORE UPDATE ON tb_possible_answers
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_exams (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    evaluator_id INT8 NOT NULL,
    name VARCHAR(200) NOT NULL,
    description TEXT NULL,
    begin_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP NULL,
    grade NUMERIC(5, 2) NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (evaluator_id)
        REFERENCES tb_people_roles (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_exams_update_updated_at_column
BEFORE UPDATE ON tb_exams
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_exams_rated (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    exam_id INT8 NOT NULL,
    rated_id INT8 NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (exam_id)
        REFERENCES tb_exams (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY (rated_id)
        REFERENCES tb_people_roles (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_exams_rated_update_updated_at_column
BEFORE UPDATE ON tb_exams_rated
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_exams_questions (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    exam_id INT8 NOT NULL,
    question_id INT8 NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (exam_id)
        REFERENCES tb_exams (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY (question_id)
        REFERENCES tb_questions (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_exams_questions_update_updated_at_column
BEFORE UPDATE ON tb_exams_questions
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();

CREATE TABLE tb_answers (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    exam_rated_id INT8 NOT NULL,
    exam_question_id INT8 NOT NULL,
    possible_answer_id INT8 NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_by INT8 NOT NULL DEFAULT 1,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_by INT8 NOT NULL DEFAULT 1,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (exam_rated_id)
        REFERENCES tb_exams_rated (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY (exam_question_id)
        REFERENCES tb_exams_questions (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY (possible_answer_id)
        REFERENCES tb_possible_answers (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

CREATE TRIGGER tb_answers_update_updated_at_column
BEFORE UPDATE ON tb_answers
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();
