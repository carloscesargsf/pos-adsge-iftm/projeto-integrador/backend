INSERT
    INTO tb_people
        (id, name, cpf, gender, email, password, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 'Admin', '58687672098', 'F', 'admin@gmail.com', '$2y$10$qhrYJwnqQCehU.WiIPS9FOnNg5bzNCG0x5l7nOmkG7/atuHdojHya', TRUE, 1, NOW(), 1, NOW()),
        (2, 'Evaluator', '14969482039', 'M', 'evaluator@gmail.com', '$2y$10$qhrYJwnqQCehU.WiIPS9FOnNg5bzNCG0x5l7nOmkG7/atuHdojHya', TRUE, 1, NOW(), 1, NOW()),
        (3, 'Rated', '84781213073', 'F', 'rated@gmail.com', '$2y$10$qhrYJwnqQCehU.WiIPS9FOnNg5bzNCG0x5l7nOmkG7/atuHdojHya', TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_companies
        (id, company_name, trading_name, cnpj, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 'Company Name', 'Trading Name', '74011557000164', TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_people_roles
        (id, person_id, company_id, role, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 1, 1, 'ADMIN', TRUE, 1, NOW(), 1, NOW()),
        (2, 2, 1, 'EVALUATOR', TRUE, 1, NOW(), 1, NOW()),
        (3, 3, 1, 'RATED', TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_categories
        (id, name, description, parent_category_id, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 'Mathematics', NULL, NULL, TRUE, 1, NOW(), 1, NOW()),
        (2, 'Algebra', NULL, 1, TRUE, 1, NOW(), 1, NOW()),
        (3, 'Logic', NULL, 1, TRUE, 1, NOW(), 1, NOW()),
        (4, 'Combinatorics', NULL, 1, TRUE, 1, NOW(), 1, NOW()),
        (5, 'Calculus and analysis', NULL, 1, TRUE, 1, NOW(), 1, NOW()),
        (6, 'Geometry and topology', NULL, 1, TRUE, 1, NOW(), 1, NOW()),
        (7, 'English', NULL, NULL, TRUE, 1, NOW(), 1, NOW()),
        (8, 'Chemistry', NULL, NULL, TRUE, 1, NOW(), 1, NOW()),
        (9, 'Biology', NULL, NULL, TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_questions_types
        (id, name, description, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 'One correct answer', NULL, TRUE, 1, NOW(), 1, NOW()),
        (2, 'One or more correct answers', NULL, TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_questions
        (id, question_type_id, category_id, title, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 1, 3, E'By using your logical reasoning skills please try to figure out which statement is true, false, or uncertain in the question below.'
                    '\r\nThe statements before will give you the clues you need to solve the problem.'
                    '\r\nThe problem consists of three statements. Based on the first two statements, the third statement may be true, false, or uncertain.'
                    '\r\n\r\nTanya is older than Eric.'
                    '\r\nCliff is older than Tanya.'
                    '\r\nEric is older than Cliff.'
                    '\r\n\r\nIf the first two statements are true, the third statement is:', TRUE, 1, NOW(), 1, NOW()),
        (2, 1, 3, E'By using your logical reasoning skills please try to figure out which statement is true, false, or uncertain in the question below.'
                    '\r\nThe statements before will give you the clues you need to solve the problem.'
                    '\r\nThe problem consists of three statements. Based on the first two statements, the third statement may be true, false, or uncertain.'
                    '\r\n\r\nBlueberries cost more than strawberries.'
                    '\r\nBlueberries cost less than raspberries.'
                    '\r\nRaspberries cost more than both strawberries and blueberries.'
                    '\r\n\r\nIf the first two statements are true, the third statement is:', TRUE, 1, NOW(), 1, NOW()),
        (3, 1, 3, E'By using your logical reasoning skills please try to figure out which statement is true, false, or uncertain in the question below.'
                    '\r\nThe statements before will give you the clues you need to solve the problem.'
                    '\r\nThe problem consists of three statements. Based on the first two statements, the third statement may be true, false, or uncertain.'
                    '\r\n\r\nAll the trees in the park are flowering trees.'
                    '\r\nSome of the trees in the park are dogwoods.'
                    '\r\nAll dogwoods in the park are flowering trees.'
                    '\r\n\r\nIf the first two statements are true, the third statement is:', TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_possible_answers
        (id, question_id, description, correct, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 1, 'True', FALSE, TRUE, 1, NOW(), 1, NOW()),
        (2, 1, 'False', TRUE, TRUE, 1, NOW(), 1, NOW()),
        (3, 1, 'Uncertain', FALSE, TRUE, 1, NOW(), 1, NOW()),
        (4, 2, 'True', TRUE, TRUE, 1, NOW(), 1, NOW()),
        (5, 2, 'False', FALSE, TRUE, 1, NOW(), 1, NOW()),
        (6, 2, 'Uncertain', FALSE, TRUE, 1, NOW(), 1, NOW()),
        (7, 3, 'True', TRUE, TRUE, 1, NOW(), 1, NOW()),
        (8, 3, 'False', FALSE, TRUE, 1, NOW(), 1, NOW()),
        (9, 3, 'Uncertain', FALSE, TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_exams
        (id, evaluator_id, name, description, begin_date, end_date, grade, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 2, 'My first exam', NULL, '2020-02-12', NULL, 20, TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_exams_rated
        (id, exam_id, rated_id, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 1, 3, TRUE, 1, NOW(), 1, NOW())
;

INSERT
    INTO tb_exams_questions
        (id, exam_id, question_id, active, created_by, created_at, updated_by, updated_at)
    VALUES
        (1, 1, 1, TRUE, 1, NOW(), 1, NOW()),
        (2, 1, 2, TRUE, 1, NOW(), 1, NOW()),
        (3, 1, 3, TRUE, 1, NOW(), 1, NOW())
;
