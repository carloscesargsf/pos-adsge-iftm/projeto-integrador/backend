# backend

## Configuring database

1. Install docker and docker-compose.
1. Inside project folder, execute this command:
```
docker-compose up -d
```

## Creating configuration on IntelliJ IDEA

* __Main class:__ com.example.demo.DemoApplication
* __VM Options:__ -Dspring.profiles.active=dev
